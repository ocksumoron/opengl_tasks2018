#include "Application.h"
#include "Mesh.h"
#include "ShaderProgram.h"

#include <iostream>
#include <vector>

class SampleApplication : public Application
{
public:
	Mesh diniSurface;

	GLuint _shaderProgram;
	GLuint _modelMatrixUniform;	
	GLuint _viewMatrixUniform;
	GLuint _projMatrixUniform;

	void makeDiniSurface(float psi, int N = -1)
	{
		if (N <= 0)
			N = _polygonsNumber;

		diniSurface.makeDiniSurface(psi, N);
		diniSurface.modelMatrix() = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f));
	}

	virtual void makeScene()
	{
		Application::makeScene();
				
		makeDiniSurface(0.28);

		_shaderProgram = ShaderProgram::createProgram("491KotelnikovaData/shader.vert", "491KotelnikovaData/shader.frag");		

		_modelMatrixUniform = glGetUniformLocation(_shaderProgram, "modelMatrix");
		_viewMatrixUniform = glGetUniformLocation(_shaderProgram, "viewMatrix");
		_projMatrixUniform = glGetUniformLocation(_shaderProgram, "projectionMatrix");
	}

	virtual void update()
	{
		Application::update();
		static float polygonsNumber = _polygonsNumber;
		if (polygonsNumber != _polygonsNumber)
		{
			makeDiniSurface(0.28, (int) _polygonsNumber);
			polygonsNumber = _polygonsNumber;
		}
		
	}

	virtual void draw()
	{
		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glUseProgram(_shaderProgram); 

		glUniformMatrix4fv(_projMatrixUniform, 1, GL_FALSE, glm::value_ptr(_projMatrix)); 
		glUniformMatrix4fv(_viewMatrixUniform, 1, GL_FALSE, glm::value_ptr(_viewMatrix)); 
		glUniformMatrix4fv(_modelMatrixUniform, 1, GL_FALSE, glm::value_ptr(diniSurface.modelMatrix())); 
		diniSurface.draw();
	}
};

int main()
{
	SampleApplication app;
	app.start();

	return 0;
}
