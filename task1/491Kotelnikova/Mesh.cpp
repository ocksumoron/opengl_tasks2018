#define _USE_MATH_DEFINES
#include <math.h>

#include <iostream>
#include <vector>

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "Mesh.h"

/**
Вспомогательный класс для добавления вершинных атрибутов в буфер
*/
template <typename T>
class Buffer : public std::vector < T >
{
public:
	void addVec2(T s, T t)
	{
		this->push_back(s);
		this->push_back(t);
	}

	void addVec3(T x, T y, T z)
	{
		this->push_back(x);
		this->push_back(y);
		this->push_back(z);
	}

	void addVec4(T r, T g, T b, T a)
	{
		this->push_back(r);
		this->push_back(g);
		this->push_back(b);
		this->push_back(a);
	}
};

//=========================================================

Mesh::Mesh() :
_primitiveType(GL_TRIANGLES),
_vao(0),
_numVertices(0)
{
}

void addDiniVertice(Buffer<float>& vertices, float psi, float theta, float phi)
{
	if (psi > 0.999)
		psi = 0.999;
	else if (psi < 0.001)
		psi = 0.001;
		
	psi = psi * M_PI;
	float sinpsi = sin(psi);
	float cospsi = cos(psi);
	float g = (phi - cospsi * theta) / sinpsi;
	float s = exp(g);
	float r = (2 * sinpsi) / (s + 1 / s);
	float t = r * (s - 1 / s) * 0.5;

	
	float x = phi - t;
	float y = r * cos(theta);
	float z = r * sin(theta);

	vertices.addVec3(x, y, z);
}

void addDiniColor(Buffer<float>& colors, float theta, float phi)
{
	float r = (cos(phi - theta) * sin(3 * theta) + 1.0f) / 2.0f;
	float g = (sin(phi * 2 + theta) * cos(2 * phi + 2) + 1.0f) / 2.0f;
	float b = (cos(4 * theta + phi) * sin(phi - theta) + 1.0f) / 2.0f;
	colors.addVec3(r, g, b);
}

void Mesh::makeDiniSurface(float psi, int N)
{
	_numVertices = 0;
	Buffer<float> vertices;
	Buffer<float> colors;
	for (int i = 0; i < N; i++)
	{
		float theta = (float)M_PI * 2 * i / N;
		float theta1 = (float)M_PI * 2 * (i + 1) / N;

		for (int j = 0; j < N; j++)
		{
			float phi = 2.0f * (float)M_PI * j / N;
			float phi1 = 2.0f * (float)M_PI * (j + 1) / N;

			//Первый треугольник, образующий квад
			addDiniVertice(vertices, psi, theta, phi);
			addDiniVertice(vertices, psi, theta, phi1);
			addDiniVertice(vertices, psi, theta1, phi1);

			addDiniColor(colors, theta, phi);
			addDiniColor(colors, theta, phi1);
			addDiniColor(colors, theta1, phi1);

			_numVertices += 3;

			//Второй треугольник, образующий квад
			addDiniVertice(vertices, psi, theta, phi);
			addDiniVertice(vertices, psi, theta1, phi1);
			addDiniVertice(vertices, psi, theta1, phi);

			addDiniColor(colors, theta, phi);
			addDiniColor(colors, theta1, phi1);
			addDiniColor(colors, theta1, phi);

			_numVertices += 3;
		}
	}

	vertices.insert(vertices.end(), colors.begin(), colors.end());

	static GLuint vbo = 0;

	if (vbo != 0)
		glDeleteBuffers(1, &vbo);

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float), vertices.data(), GL_STATIC_DRAW);

	if (_vao != 0)
		glDeleteVertexArrays(1, &_vao);

	_vao = 0;
	glGenVertexArrays(1, &_vao);
	glBindVertexArray(_vao);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)(_numVertices * 3 * 4)); //сдвиг = число вершин * число компонентов (x, y, z) * размер одного компонента (float 4 байта)

	glBindVertexArray(0);
}

void Mesh::draw()
{
	glBindVertexArray(_vao);
	glDrawArrays(_primitiveType, 0, _numVertices);
}
