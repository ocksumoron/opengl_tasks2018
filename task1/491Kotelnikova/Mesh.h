#pragma once

#include <GL/glew.h>

#include <string>
#include <memory>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

/**
Описание меша:
- тип примитива (обычно GL_TRIANGLES)
- идентификатор Vertex Array Object (хранит настройки буферов и вершинных атрибутов)
- количество вершин в модели
- матрица модели (LocalToWorld)
*/
class Mesh
{
public:
	Mesh();

	GLuint& primitiveType() { return _primitiveType; }

	/**
	Возвращает матрицу модели, которая описывает положение меша в мировой системе координат
	*/
	glm::mat4& modelMatrix() { return _modelMatrix; }

	/**
	Запускает отрисовку меша
	*/
	void draw();

	//----------------- Метод для инициализации меша
	void makeDiniSurface(float psi, int N);

protected:
	GLuint _primitiveType;
	GLuint _vao;
	unsigned int _numVertices;
	glm::mat4 _modelMatrix;
};