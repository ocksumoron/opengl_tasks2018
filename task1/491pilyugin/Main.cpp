#include <iostream>

#include <ShaderProgram.hpp>
#include <Application.hpp>

#include "LSystem.h"
#include <Common.h>
#include "cylinder.h"

class TreeApplication: public Application {
  public:
   LSystem lTree;
   std::vector<float> cylinderCoord;
   const int numberPolygon = 90;

   ShaderProgramPtr _shader;
   std::vector<MeshPtr> cylinderArray;

   MeshPtr makeRotateCylinder(glm::vec3 start, glm::vec3 end, float radius, float radiusScale, bool rotateFlag) {
    float height = glm::distance(start, end);
    glm::mat4 modelMatrix;
    glm::vec3 pr(end.x, end.y, start.z);
    // сдвигаем на start точку
    glm::mat4 transMat = glm::translate(glm::mat4(1.0f), start);

    // считаем ось и угол, на который надо повернуть конус
    if (pr != start) {
        float sin = glm::distance(pr, start) / height;
        float angle = std::asin(sin);
        glm::vec3 axis = glm::cross(glm::vec3(0, 0, 1), pr - start);
        // вращаем на нужный угол вокруг оси
        modelMatrix = glm::rotate(transMat, angle, axis);
    } else {
        modelMatrix = transMat;
    }

   MeshPtr cylinder = makeCylinder(radius, radiusScale, height, numberPolygon, rotateFlag);
   cylinder->setModelMatrix(modelMatrix);
   return cylinder;
   }

  TreeApplication(): Application() {}

  void makeScene() override {
    Application::makeScene();

    _cameraMover = std::make_shared<FreeCameraMover>();

    for (int i = 0; i < cylinderCoord.size(); i+=8) {
        glm::vec3 start = glm::vec3(cylinderCoord[i], cylinderCoord[i+1], cylinderCoord[i+2]);
        glm::vec3 end = glm::vec3(cylinderCoord[i+4], cylinderCoord[i+5], cylinderCoord[i+6]);
        //при больших углах нужно перевернуть конус
        bool rotateFlag = swapIfNeed(start, end);
        cylinderArray.push_back(makeRotateCylinder(start, end, cylinderCoord[i+3], lTree.radiusScale, rotateFlag));
    }

    //Создаем шейдерную программу
    _shader = std::make_shared<ShaderProgram>("491pilyuginData/shaderNormal.vert",
    "491pilyuginData/shader.frag");
  }

  // void update() override
  // {
  //     Application::update();
  // }

  void draw() override
  {
    Application::draw();

    int width, height;
    glfwGetFramebufferSize(_window, &width, &height);

    glViewport(0, 0, width, height);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //Устанавливаем шейдер.
    _shader->use();

    //Устанавливаем общие юниформ-переменные
    _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
    _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

    //Рисуем meshes
    for (auto cylin: cylinderArray) {
      _shader->setMat4Uniform("modelMatrix", cylin->modelMatrix());
      cylin->draw();
    }
  }

  bool swapIfNeed(glm::vec3& a, glm::vec3& b){
    if (b[2] < a[2]) {
      glm::vec3 tmp = a;
      a = b;
      b = tmp;
      return 1;
    } else {
      return 0;
    }
  }

  void initLSystem(
  std::map <char, std::string>	rules,
  std::string	initialString,
  float	startAngle,
  float startDistance,
  float startRadius,
  float	distScale,
  float	angleScale,
  float radiusScale,
  std::string	currentString,
  int numIterations) {

    lTree = LSystem (rules, initialString,
                    startAngle, startDistance, startRadius, distScale,
                    angleScale, radiusScale, currentString,
                    numIterations);
    lTree.buildSystem();
    cylinderCoord = lTree.calculateLineCoords();
  }

};

int main(int argc, char** argv)
{
  TreeApplication app;
  // std::string initialString = "AB";
  std::string initialString = "F";
  std::map<char, std::string> rules;
  rules['F'] = "F[-F][^F]<[F]>[+F][&F][+F][F]";
  // rules['A'] = "[F[+FCA][-FCA]]";
  // rules['B'] = "[F[>FCB][<FCB]]";
  float startAngle = 70 * 3.1416 / 180;
  float startDistance = 1;
  float startRadius = 0.2;
  float distanceScale = 0.75;
  float angleScale = 0.8;
  float radiusScale = 0.55;
  int numIterations = 4;

 app.initLSystem(
   rules,
   initialString,
   startAngle,
   startDistance,
   startRadius,
   distanceScale,
   angleScale,
   radiusScale,
   initialString,
   numIterations);

  app.start();

  return 0;
}
