#include "Mesh.hpp"

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <iostream>
#include <vector>

glm::vec3 getBottleEquation(float a, float v, float u)
{
    return glm::vec3((a + sin(v) * cos(u / 2)  - sin(u / 2) * sin(2 * v)) * cos(u),
                     (a + sin(v) * cos(u / 2) - sin(u / 2) * sin(2 * v)) * sin(u),
                     sin(u / 2) * sin(v) + cos(u / 2) * sin(2 * v));
}

glm::vec3 getBottleNormal(float a, float v, float u) {
    // N = [r_u, r_v]
    glm::vec3 r_v = glm::vec3((cos(u / 2) * cos(v) - 2 * sin(u / 2) * cos(2 * v)) * cos(u),
                              (cos(u / 2) * cos(v) - 2 * sin(u / 2) * cos(2 * v)) * sin(u),
                              (cos(u / 2) * cos(v) + 2 * sin(u / 2) * cos(2 * v)));

    glm::vec3 r_u = glm::vec3((-0.5 * sin(u / 2) * sin(v) - 0.5 * cos(u / 2) * sin(2 * v)) * cos(u) -
                                  (a + cos(u / 2) * sin(v) - sin(u / 2) * sin(2 * v)) * sin(u),
                              (-0.5 * sin(u / 2) * sin(v) - 0.5 * cos(u / 2) * sin(2 * v)) * sin(u) +
                                  (a + cos(u / 2) * sin(v) - sin(u / 2) * sin(2 * v)) * cos(u),
                              0.5 * cos(u / 2) * sin(v) - 0.5 * sin(u / 2) * sin(2 * v));
    glm::vec3 N = glm::cross(r_u, r_v);
    glm::normalize(N);
    return N;
}

MeshPtr makeBottle(float radius, unsigned int N)
{
    if (radius <= 0.1) {
        radius = 3.0;
    }

    if (N < 5) {
        N = 5;
    }

    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;

    for (unsigned int i = 0; i < N; i++)
    {
        float v = 2.0f * glm::pi<float>() * i / N;
        float v_1 = 2.0f * glm::pi<float>() * (i + 1) / N;

        for (unsigned int j = 0; j < N; j++)
        {
            float u = 2.0f * glm::pi<float>() * j / N;
            float u_1 = 2.0f * glm::pi<float>() * (j + 1) / N;

            // Прямой треугольник

            vertices.push_back(getBottleEquation(radius, u, v));
            vertices.push_back(getBottleEquation(radius, u_1, v));
            vertices.push_back(getBottleEquation(radius, u, v_1));

            normals.push_back(getBottleNormal(radius, u, v));
            normals.push_back(getBottleNormal(radius, u_1, v));
            normals.push_back(getBottleNormal(radius, u, v_1));

            //Обратный треугольник

            vertices.push_back(getBottleEquation(radius, u_1, v));
            vertices.push_back(getBottleEquation(radius, u, v_1));
            vertices.push_back(getBottleEquation(radius, u_1, v_1));

            normals.push_back(getBottleNormal(radius, u_1, v));
            normals.push_back(getBottleNormal(radius, u, v_1));
            normals.push_back(getBottleNormal(radius, u_1, v_1));
        }
    }

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());


    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}


