#version 330

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;

out vec4 color;

void main()
{
	float eps = 0.00000001;
	if(vertexPosition.x * vertexPosition.x  <= eps ||
	   vertexPosition.y * vertexPosition.y  <= eps ||
	   vertexPosition.z * vertexPosition.z  <= eps ) {
		color.rgb = vec3(1.0, 1.0, 1.0);
	} else {
		color.rgb = (vertexNormal.xyz + 1.0) * 0.5;
	}
    color.a = 1.0;

    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
}


