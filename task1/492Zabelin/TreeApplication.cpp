#include "TreeApplication.h"


void TreeApplication::setupLSystem(
    LSystem& system,
    uint16_t numIterations
) {
    system.executeIterations(numIterations);
    conesParams = system.getConesParams();
}


void TreeApplication::makeScene()
{
    Application::makeScene();
    cones.reserve(conesParams.size());
    for (const auto& params: conesParams)
        cones.push_back(getCone(params));
    //Создаем шейдерную программу
    shader = std::make_shared<ShaderProgram>(
        "492ZabelinData/shaderNormal.vert",
        "492ZabelinData/simpleShader.frag"
    );
}

void TreeApplication::draw()
{
    Application::draw();

    //Получаем размеры экрана (окна)
    int width, height;
    glfwGetFramebufferSize(_window, &width, &height);

    //Устанавливаем порт вывода на весь экран (окно)
    glViewport(0, 0, width, height);

    //Очищаем порт вывода (буфер цвета и буфер глубины)
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //Устанавливаем шейдер.
    shader->use();

    //Устанавливаем общие юниформ-переменные
    shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
    shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

    //Рисуем дерево
    for (const auto& cone: cones) {
        shader->setMat4Uniform("modelMatrix", cone->modelMatrix());
        cone->draw();
    }
}
