#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include "Maze.h"
#include <glm/glm.hpp>
#include "Camera.hpp"

#include <iostream>
#include <vector>

/**
Несколько примеров шейдеров
*/
class MazeApp : public Application
{
public:
    vector<MeshPtr> meshes;
    vector<CameraMoverPtr> movers = {std::make_shared<OrbitCameraMover>(), std::make_shared<FreeCameraMover>()};
    int currentIndex = 0;
    Maze maze;

    ShaderProgramPtr shader;

    MazeApp() : maze(10),
        Application(nullptr) {
            _cameraMover = movers[0];
    }

    void makeScene() override
    {
        Application::makeScene();

        maze.generate();
        maze.debug();
        meshes = maze.get_meshes();
        meshes.push_back(makeFloor(0));
        for (MeshPtr mesh : meshes) {
            mesh->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));
        }

        //=========================================================

        shader = std::make_shared<ShaderProgram>("492ZinovData/shader.vert", "492ZinovData/shader.frag");
    }

    void checkUpdate() override
    {
        if (currentIndex == 1) {
            maze.clamp_pos(std::dynamic_pointer_cast<FreeCameraMover>(movers[1])->_pos);
        }
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("Maze", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            ImGui::RadioButton("orbit camera", &currentIndex, 0);
            ImGui::RadioButton("first person camera", &currentIndex, 1);
        }
        ImGui::End();
    }

    void draw() override
    {
        Application::draw();
        Application::setCameraMover(movers[currentIndex]);

        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        shader->use();

        //Загружаем на видеокарту значения юниформ-переменные: время и матрицы
        shader->setFloatUniform("time", (float)glfwGetTime()); //передаем время в шейдер

        shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        for (MeshPtr mesh : meshes) {
            shader->setMat4Uniform("modelMatrix", mesh->modelMatrix());
            mesh->draw();
        }
    }
};

int main()
{
    MazeApp app;
    app.start();
    app.maze.debug();

    return 0;
}
