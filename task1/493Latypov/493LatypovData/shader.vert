#version 450

uniform mat4 model_matrix;
uniform mat4 view_matrix;
uniform mat4 projection_matrix;

layout(location = 0) in vec3 vertex_position;
layout(location = 1) in vec3 vertex_normal;

out vec4 color;

void main()
{
	color = vec4(0.5, 0.5, 0.5, 1.0);
	color.r = color.r + vertex_position.z * 1;
	color.g = color.g + vertex_position.z * 1;
	color.b = color.b - vertex_position.z * 2;
    gl_Position = projection_matrix * view_matrix * model_matrix * vec4(vertex_position, 1.0);
}