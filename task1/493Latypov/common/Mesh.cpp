#include "Mesh.hpp"
#include "PerlinNoise.hpp"

#include <iostream>

typedef std::vector<std::vector<std::vector<std::pair<int, glm::vec3>>>> NormalsData;

MeshPtr calculateSurface(float size, unsigned int frequency, int num_octaves, float persistence) {

    std::vector<glm::vec3> vertices;
    // TODO Get it
    NormalsData normals_ij(frequency + 2,
                           std::vector<std::vector<std::pair<int, glm::vec3>>>(frequency + 2,
                                                                               std::vector<std::pair<int, glm::vec3>>()));

    PerlinNoise noise = PerlinNoise(num_octaves, persistence);
    float step = 2.0f / frequency;

    // Заполняем normals_ij
    int it = 0;
    int ii = 0;
    int jj = 0;
    for (auto i = -1.0F; i < 1.0F; i += step, ++ii) {
        jj = 0;
        for (auto j = -1.0F; j < 1.0F; j += step, ++jj) {
            // Первый треугольник, first, second, third - его вершины
            // x, y получаются из i, j (+ step), z из perlinNoise2D
            glm::vec3 first = glm::vec3(i * size, j * size, noise.perlinNoise2D(i, j)); // вершина 1
            glm::vec3 second = glm::vec3((i + step) * size, j * size, noise.perlinNoise2D(i + step, j)); // вершина 2
            glm::vec3 third = glm::vec3(i * size, (j + step) * size, noise.perlinNoise2D(i, j + step)); // вершина 3
            // Добавляем координаты треугольника в общий список вершин
            vertices.push_back(first);
            vertices.push_back(second);
            vertices.push_back(third);

            // Вычисляем нормали к данному треугольнику
            // glm::cross - векторное произведение
            // glm::normalize - нормализация длины полученного перпендикуляра
            glm::vec3 normal = glm::normalize(glm::cross(third - first, second - first));
            // Данные нормали дальше будут интерполироваться, поэтому пока храним их в normals_ij
            normals_ij[ii][jj].emplace_back(it++, -normal);
            normals_ij[ii + 1][jj].emplace_back(it++, -normal);
            normals_ij[ii][jj + 1].emplace_back(it++, -normal);

            // Аналогично разбирается второй треугольник
            first = glm::vec3(i * size, (j + step) * size, noise.perlinNoise2D(i, j + step));
            second = glm::vec3((i + step) * size, (j + step) * size, noise.perlinNoise2D(i + step, j + step));
            third = glm::vec3((i + step) * size, j * size, noise.perlinNoise2D(i + step, j));
            vertices.push_back(first);
            vertices.push_back(second);
            vertices.push_back(third);

            // Вычисляем нормали к данному треугольнику
            normal = glm::normalize(glm::cross(third - first, second - first));
            normals_ij[ii][jj + 1].emplace_back(it++, normal);
            normals_ij[ii + 1][jj + 1].emplace_back(it++, normal);
            normals_ij[ii + 1][jj].emplace_back(it++, normal);
        }
    }

    // Вычисление интерполированных нормалей (ОТВЕТ)
    // Нормалей у нас будет it штук, поскольку
    auto normals = std::vector<glm::vec3>(static_cast<unsigned long>(it));
    for (ii = 0; ii < frequency + 1; ++ii) {
        for (jj = 0; jj < frequency + 1; ++jj) {
            glm::vec3 normal;
            for (auto &elem : normals_ij[ii][jj]) {
                normal += elem.second;
            }
            normal /= normals_ij[ii][jj].size();
            for (auto &elem : normals_ij[ii][jj]) {
                normals[elem.first] = normal;
            }
        }
    }

    // Создаем буфер для данных о координатах вершин
    std::shared_ptr<DataBuffer> buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    // Создаем буфер для данных о нормалях
    std::shared_ptr<DataBuffer> buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    // Создаем Mesh для рендеринга данных из буферов
    MeshPtr mesh = std::make_shared<Mesh>();
    // Добавляем в Mesh наши аттрибуты - координаты и нормали
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, nullptr, buf1);
    // Устанавливаем тип примитива - треугольник
    mesh->setPrimitiveType(GL_TRIANGLES);
    // Устанавливаем число вершин
    mesh->setVertexCount(static_cast<GLuint>(vertices.size()));
    return mesh;
}
