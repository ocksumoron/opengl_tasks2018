#pragma once

#define GLM_FORCE_RADIANS

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <GL/glew.h>

#include <string>
#include <map>
#include <memory>

class DataBuffer {
public:
    explicit DataBuffer(GLenum buffer_type = GL_ARRAY_BUFFER) : buffer_type_(buffer_type) {
        glGenBuffers(1, &vbo_);
    }

    ~DataBuffer() {
        glDeleteBuffers(1, &vbo_);
    }

    DataBuffer(const DataBuffer &) = delete;

    void operator=(const DataBuffer &) = delete;

    void setData(GLsizeiptr size, const GLvoid *data) {
        glBindBuffer(buffer_type_, vbo_);
        glBufferData(buffer_type_, size, data, GL_DYNAMIC_DRAW);
        glBindBuffer(buffer_type_, 0);
    }

    void bind() const {
        glBindBuffer(buffer_type_, vbo_);
    }

    void unbind() const {
        glBindBuffer(buffer_type_, 0);
    }

protected:
    GLuint vbo_{};
    GLenum buffer_type_;
};

class Mesh {
public:
    Mesh() : primitive_type_(GL_TRIANGLES), vertex_count_(0) {
        glCreateVertexArrays(1, &vao_);
    }

    ~Mesh() {
        glDeleteVertexArrays(1, &vao_);
    }

    Mesh(const Mesh &) = delete;

    void operator=(const Mesh &) = delete;

    void setAttribute(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride,
                      const GLvoid *pointer, const std::shared_ptr<DataBuffer> &buffer) {
        _buffers[index] = buffer;
        glBindVertexArray(vao_);
        buffer->bind();
        glEnableVertexArrayAttrib(vao_, index);
        glVertexAttribPointer(index, size, type, normalized, stride, pointer);
        buffer->unbind();
        glBindVertexArray(0);
    }

    // Устанавливает тип примитива (GL_POINTS, GL_LINES, GL_TRIANGLES и другие)
    void setPrimitiveType(GLuint type) { primitive_type_ = type; }

    // Устанавливает количество вершин, которые должны быть отрендерены
    void setVertexCount(GLuint count) { vertex_count_ = count; }

    // Матрица модели (преобразует локальные координаты в мировые)
    glm::mat4 GetModelMatrix() const { return model_matrix_; }

    // Устанавливает матрицу модели
    void setModelMatrix(const glm::mat4 &m) { model_matrix_ = m; }

    // Рисует модель
    void draw() const {
        glBindVertexArray(vao_);
        glDrawArrays(primitive_type_, 0, vertex_count_);
    }

protected:
    // Идентификатор Vertex Array Object
    GLuint vao_{};

    // Буферы с данными - храним здесь, чтобы они не были удалены раньше модели
    std::map<GLuint, std::shared_ptr<DataBuffer>> _buffers;

    // Тип геометрического примитива
    GLuint primitive_type_;

    // Количество вершин в модели
    GLuint vertex_count_;

    // Матрица модели (local to world)
    glm::mat4 model_matrix_;
};

typedef std::shared_ptr<Mesh> MeshPtr;

MeshPtr calculateSurface(float size = static_cast<float>(1.),
                         unsigned int frequency = 100,
                         int num_octaves = 1,
                         float persistence = 0.5f);