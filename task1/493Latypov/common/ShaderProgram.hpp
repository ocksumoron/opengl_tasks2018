#pragma once

#define GLM_FORCE_RADIANS

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <GL/glew.h>

#include <string>
#include <vector>
#include <memory>


// Класс для создания и работы с отдельным шейдером
class Shader {
public:
    explicit Shader(GLenum shader_type) :
            shader_type_(shader_type),
            id_(glCreateShader(shader_type)) {
    }

    Shader(const Shader &) = delete;

    void operator=(const Shader &) = delete;

    ~Shader() {
        glDeleteShader(id_);
    }

    // Читает текст шейдера из файла
    void createFromFile(const std::string &file_path);

    // Использует текст шейдера из строковой переменной
    void createFromString(const std::string &text);

    // Возвращает идентификатор шейдера
    GLuint id() const { return id_; }

protected:
    GLuint id_;
    GLenum shader_type_;
};

// Класс для работы с шейдерной программой
class ShaderProgram {
public:
    ShaderProgram() :
            program_id_(glCreateProgram()) {
    }

    ShaderProgram(const std::string &vertFilepath, const std::string &fragFilepath) :
            program_id_(glCreateProgram()) {
        createProgram(vertFilepath, fragFilepath);
    }

    ~ShaderProgram() {
        glDeleteProgram(program_id_);
    }

    ShaderProgram(const ShaderProgram &) = delete;

    void operator=(const ShaderProgram &) = delete;

    // Создает шейдерную программу из нескольких шейдеров: вершинного и фрагментного
    void createProgram(const std::string &vert_file_path, const std::string &frag_file_path);

    // Добавляет шейдер к программе
    void attachShader(const std::shared_ptr<Shader> &shader);

    // Линкует программу
    void linkProgram();

    GLuint GetId() const { return program_id_; }

    void use() const {
        glUseProgram(program_id_);
    }

    void setIntUniform(const std::string &name, const int &value) const {
        GLint uniformLoc = glGetUniformLocation(program_id_, name.c_str());
        glProgramUniform1i(program_id_, uniformLoc, value);
    }

    void setFloatUniform(const std::string &name, const float &value) const {
        GLint uniformLoc = glGetUniformLocation(program_id_, name.c_str());
        glProgramUniform1f(program_id_, uniformLoc, value);
    }

    void setVec2Uniform(const std::string &name, const glm::vec2 &vec) const {
        GLint uniformLoc = glGetUniformLocation(program_id_, name.c_str());
        glProgramUniform2fv(program_id_, uniformLoc, 1, glm::value_ptr(vec));
    }

    void setVec3Uniform(const std::string &name, const glm::vec3 &vec) const {
        GLint uniformLoc = glGetUniformLocation(program_id_, name.c_str());
        glProgramUniform3fv(program_id_, uniformLoc, 1, glm::value_ptr(vec));
    }

    void setVec4Uniform(const std::string &name, const glm::vec4 &vec) const {
        GLint uniformLoc = glGetUniformLocation(program_id_, name.c_str());
        glProgramUniform4fv(program_id_, uniformLoc, 1, glm::value_ptr(vec));
    }

    void setMat3Uniform(const std::string &name, const glm::mat3 &mat) const {
        GLint uniformLoc = glGetUniformLocation(program_id_, name.c_str());
        glProgramUniformMatrix3fv(program_id_, uniformLoc, 1, GL_FALSE, glm::value_ptr(mat));
    }

    void setMat4Uniform(const std::string &name, const glm::mat4 &mat) const {
        GLint uniformLoc = glGetUniformLocation(program_id_, name.c_str());
        glProgramUniformMatrix4fv(program_id_, uniformLoc, 1, GL_FALSE, glm::value_ptr(mat));
    }

    void setVec3UniformArray(const std::string &name, const std::vector<glm::vec3> &positions) const {
        GLint uniformLoc = glGetUniformLocation(program_id_, name.c_str());
        glProgramUniform3fv(program_id_, uniformLoc, static_cast<GLsizei>(positions.size()),
                     reinterpret_cast<const GLfloat *>(positions.data()));
    }

protected:
    GLuint program_id_;
    std::vector<std::shared_ptr<Shader>> shaders_;
};