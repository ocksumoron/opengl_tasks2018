set(COMMON_SRC
        common/Application.cpp
        common/DebugOutput.cpp
        common/Camera.cpp
        common/Mesh.cpp
        common/ShaderProgram.cpp)


set(MAZE_SRC
        include/CreateMaze.h
        include/MazeApplication.h
        include/MazeWalkerCamera.h

        src/Maze.h
        src/Maze.cpp
        src/CreateMaze.cpp
        src/MazeApplication.cpp
        src/MazeWalkerCamera.cpp
        )

set(SRC_FILES
        ${COMMON_SRC}
        ${MAZE_SRC}
        main.cpp
        )

include_directories(common)

MAKE_TASK(496Karyamin 1 "${SRC_FILES}")
