//
// Created by avk on 14.03.18.
//

#include "../include/CreateMaze.h"

std::shared_ptr<Maze> createTestMaze() {
    typedef Point P;
    typedef Wall W;

    std::size_t xSize = 3, ySize = 3;

    auto res = std::make_shared<Maze>(xSize, ySize);

    std::vector<W> walls = {
             W(0,0,0,0,3,1),
             W(3,0,0,3,3,1),

             W(1,0,0,3,0,1),
             W(0,1,0,2,1,1),
             W(1,2,0,3,2,1),
             W(0,3,0,2,3,1),
    };

    res->addWalls(walls);
    return res;
}