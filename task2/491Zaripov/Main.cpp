#include "Application.hpp"
#include "Mesh.hpp"
#include "ShaderProgram.hpp"
#include "Texture.hpp"
#include "LightInfo.hpp"

#include <iostream>


class TestApplication : public Application {
public:
	void makeScene() override {

		Application::makeScene();

		_mesh = makeRelief();
		_mesh->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));

		_cameraMover = std::make_shared<FirstPersonCameraMover>(_mesh);

		_shader = std::make_shared<ShaderProgram>("491ZaripovData/shader.vert", "491ZaripovData/shader.frag");

		_light.ambient = glm::vec3(0.2, 0.2, 0.2);
		_light.diffuse = glm::vec3(0.8, 0.8, 0.8);
		_light.specular = glm::vec3(0.2, 0.2, 0.2);

		_snow = loadTexture("491ZaripovData/snow.jpg");
		_grass = loadTexture("491ZaripovData/grass.jpg");
		_sand = loadTexture("491ZaripovData/sand.jpg");
        _stone = loadTexture("491ZaripovData/stone.jpg");
		_map = loadTexture("491ZaripovData/map.jpg");
		_specular = loadTexture("491ZaripovData/specular.jpg");

		glGenSamplers(1, &_sampler);
		glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
	}

	void updateGUI() override
	{
		Application::updateGUI();

		ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
		if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
		{
			ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

			if (ImGui::CollapsingHeader("Light"))
			{
				ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
				ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
			}
		}
		ImGui::End();
	}

	void draw() override {
		Application::draw();

		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		_shader->use();
		_shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
		_shader->setMat4Uniform("modelMatrix", _mesh->modelMatrix());
		_shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _mesh->modelMatrix()))));
        glm::vec3 lightDir = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta));
		glm::vec4 lightDirCamSpace = glm::vec4(_camera.viewMatrix * glm::vec4(lightDir,0.0));
		_shader->setVec4Uniform("lightDirCamSpace", lightDirCamSpace);
		_shader->setVec3Uniform("light.La", _light.ambient);
		_shader->setVec3Uniform("light.Ld", _light.diffuse);
		_shader->setVec3Uniform("light.Ls", _light.specular);

		glActiveTexture(GL_TEXTURE0);
		glBindSampler(0, _sampler);
		_snow->bind();
		_shader->setIntUniform("snowTex", 0);

		glActiveTexture(GL_TEXTURE0 + 1);
		_grass->bind();
		_shader->setIntUniform("grassTex", 1);

		glActiveTexture(GL_TEXTURE0 + 2);
		_sand->bind();
		_shader->setIntUniform("sandTex", 2);

		glActiveTexture(GL_TEXTURE0 + 3);
		_stone->bind();
		_shader->setIntUniform("stoneTex", 3);

		glActiveTexture(GL_TEXTURE0 + 4);
		_map->bind();
		_shader->setIntUniform("mapTex", 4);

		glActiveTexture(GL_TEXTURE0 + 5);
		_specular->bind();
		_shader->setIntUniform("specularTex", 5);

		_mesh->draw();

		glBindSampler(0, 0);
		glUseProgram(0);
	}

protected:
	TerrainPtr _mesh;
	ShaderProgramPtr _shader;

	float _phi = 0.0f;
	float _theta = glm::pi<float>() * 0.25f;

    glm::vec3 _lightAmbientColor;
	glm::vec3 _lightDiffuseColor;

	TexturePtr _snow, _grass, _sand, _stone, _map, _specular;
	GLuint _sampler;
	LightInfo _light;
};

#include <vector>

int main() {
	TestApplication app;
	app.start();

	return 0;
}
