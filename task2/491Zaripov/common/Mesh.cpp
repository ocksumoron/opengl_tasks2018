
#include <Mesh.hpp>

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <iostream>
#include <vector>

TerrainPtr makeRelief() {
    const unsigned int M = 200;

    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;

    // Генерация рельефа
    Mat<double> z0(M+1, M+1);
    Mat<double> z(M+1, M+1);

    for (unsigned int i = 0; i < M+1; ++i) {
        for (unsigned int j = 0; j < M+1; ++j) {
            z0[i][j] = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
            z[i][j] = z0[i][j];
        }
    }

    const double kernel[3][3] = {{0.077847,0.123317,0.077847},{0.123317,0.195346,0.123317},{0.077847,0.123317,0.077847}};

    for (unsigned int pass = 0; pass < 50; ++pass) {
        for (unsigned int i = 1; i < M; ++i) {
            for (unsigned int j = 1; j < M; ++j) {
                double s = 0;
                s += z0[i-1][j-1] * kernel[0][0];
                s += z0[i-1][j+0] * kernel[0][1];
                s += z0[i-1][j+1] * kernel[0][2];
                s += z0[i+0][j-1] * kernel[1][0];
                s += z0[i+0][j+0] * kernel[1][1];
                s += z0[i+0][j+1] * kernel[1][2];
                s += z0[i+1][j-1] * kernel[2][0];
                s += z0[i+1][j+0] * kernel[2][1];
                s += z0[i+1][j+1] * kernel[2][2];
                z[i][j] = s;
            }
        }

        for (unsigned int i = 0; i < M+1; ++i)
            for (unsigned int j = 0; j < M+1; ++j)
                z0[i][j] = z[i][j];
    }

    for (unsigned int i = 0; i < M+1; ++i) {
        z[i][0] = 0;
        z[0][i] = 0;
        z[i][M] = 0;
        z[M][i] = 0;
    }
    std::vector<glm::vec2> tex_coords;
    std::vector<glm::vec2> map_coords;
    Mat<glm::vec3> norms(M+1, M+1);
    // Добавляем вершины и нормали
    for (unsigned int i = 0; i < M; ++i) {
        for (unsigned int j = 0; j < M; ++j) {
            const double x0 = -1.0 + 2.0 * i / M;
            const double y0 = -1.0 + 2.0 * j / M;
            const double x1 = -1.0 + 2.0 * (i + 1) / M;
            const double y1 = -1.0 + 2.0 * (j + 1) / M;

            vertices.push_back(glm::vec3(x0, y0, z[i][j]));
            vertices.push_back(glm::vec3(x1, y0, z[i+1][j]));
            vertices.push_back(glm::vec3(x0, y1, z[i][j+1]));

            glm::vec3 n1 = -glm::normalize(glm::cross(vertices[vertices.size()-1] - vertices[vertices.size()-3], vertices[vertices.size()-2] - vertices[vertices.size()-3]));

            norms[i][j] += n1;
            norms[i+1][j] += n1;
            norms[i][j+1] += n1;

            vertices.push_back(glm::vec3(x0, y1, z[i][j+1]));
            vertices.push_back(glm::vec3(x1, y1, z[i+1][j+1]));
            vertices.push_back(glm::vec3(x1, y0, z[i+1][j]));

            n1 = glm::normalize(glm::cross(vertices[vertices.size()-1] - vertices[vertices.size()-3], vertices[vertices.size()-2] - vertices[vertices.size()-3]));

            norms[i][j+1] += n1;
            norms[i+1][j+1] += n1;
            norms[i+1][j] += n1;

            float tx0 = (x0 + 1.f) * 8.f;
            float ty0 = (y0 + 1.f) * 8.f;
            float tx1 = (x1 + 1.f) * 8.f;
            float ty1 = (y1 + 1.f) * 8.f;
            tex_coords.push_back(glm::vec2(tx0, ty0));
            tex_coords.push_back(glm::vec2(tx1, ty0));
            tex_coords.push_back(glm::vec2(tx0, ty1));

            tex_coords.push_back(glm::vec2(tx0, ty1));
            tex_coords.push_back(glm::vec2(tx1, ty1));
            tex_coords.push_back(glm::vec2(tx1, ty0));

            float x_map0 = (x0 + 1.f) / 2.f;
            float y_map0 = (y0 + 1.f) / 2.f;
            float x_map1 = (x1 + 1.f) / 2.f;
            float y_map1 = (y1 + 1.f) / 2.f;
            map_coords.push_back(glm::vec2(x_map0, y_map0));
            map_coords.push_back(glm::vec2(x_map1, y_map0));
            map_coords.push_back(glm::vec2(x_map0, y_map1));

            map_coords.push_back(glm::vec2(x_map0, y_map1));
            map_coords.push_back(glm::vec2(x_map1, y_map1));
            map_coords.push_back(glm::vec2(x_map1, y_map0));
        }
    }

    for (unsigned int i = 0; i <= M; ++i)
        for (unsigned int j = 0; j <= M; ++j)
            norms[i][j] = glm::normalize(norms[i][j]);

    for (unsigned int i = 0; i < M; ++i) {
        for (unsigned int j = 0; j < M; ++j) {
            normals.push_back(norms[i][j]);
            normals.push_back(norms[i+1][j]);
            normals.push_back(norms[i][j+1]);

            normals.push_back(norms[i][j+1]);
            normals.push_back(norms[i+1][j+1]);
            normals.push_back(norms[i+1][j]);
        }
    }

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(tex_coords.size() * sizeof(float) * 2, tex_coords.data());

    DataBufferPtr buf3 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf3->setData(map_coords.size() * sizeof(float) * 2, map_coords.data());

    TerrainPtr mesh = std::make_shared<Terrain>(M, z);
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setAttribute(3, 2, GL_FLOAT, GL_FALSE, 0, 0, buf3);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}