#include "LSystem.h"

LSystem::LSystem() {};

LSystem::LSystem (
		std::map <char, std::string>	rules,
		std::string	initialString,
		float	startAngle,
		float startDistance,
		float startRadius,
		float	distScale,
		float	angleScale,
		float radiusScale,
		std::string	currentString,
		int numIterations
	) :
	rules(rules),
	initialString(initialString),
	startAngle(startAngle),
	startDistance(startDistance),
	startRadius(startRadius),
	distScale(distScale),
	angleScale(angleScale),
	radiusScale(radiusScale),
	currentString(currentString),
	numIterations(numIterations) {}


std::vector<float> LSystem::calculateLineCoords() {
	std::vector<float> coord;
	State curState;
	initState(curState, glm::vec3(0, -0.9, 0), startDistance, startRadius, glm::vec3(0, 0, 0), startAngle, 1);
	std::stack<State> stateStack;

	for (int i = 0; i < currentString.length(); i++)
	{
		switch (currentString[i])
		{
			case '[':		// push state
				stateStack.push(curState);
				break;

			case ']':		// pop state
				curState = stateStack.top();
				stateStack.pop();
				break;

			case 'F':
				addNewCoord(curState, coord);
				updateState(curState);
				addNewCoord(curState, coord);
				break;

			case 'f':
				updateState(curState);
				break;

			case '!':						// inverse + and - meaing (just as for & and ^ and < and >)
				curState.invert *= -1;
				break;

			case '+':
				curState.angles.z += curState.invert*curState.angle;
				break;

			case '-':
				curState.angles.z -= curState.invert*curState.angle;
				break;

			case '&':
				curState.angles.x += curState.invert*curState.angle;
				break;

			case '^':
				curState.angles.x -= curState.invert*curState.angle;
				break;

			case '<':
				curState.angles.y += curState.invert*curState.angle;
				break;

			case '>':
				curState.angles.y -= curState.invert*curState.angle;
				break;
		}
	}
	return coord;
}
// построение строки, соотвествующей конечному состоянию системы
void LSystem::buildSystem() {
	for(int i = 0; i < numIterations; i++) {
		currentString = makeOneStep(currentString);
	}
}

std::string LSystem::makeOneStep (const std::string& in) {
	std::string out;
	for (auto chr: in) {
		std::map<char,std::string>::iterator rule = rules.find(chr);
		if (rule != rules.end())
			out += rule->second;
		else
			out += chr;
	}

	return out;
}

void LSystem::updateState(State& state) {
	state.pos = getNewPosition(state);
	state.angle *= angleScale;
	state.stepDistance *= distScale;
	state.radius *= radiusScale;
}

glm::vec3 LSystem::getNewPosition(State& state) {
	glm::vec3 vect;
	vect = glm::vec3(0, state.stepDistance, 0);
	vect = glm::rotateX(vect, state.angles.x);
	vect = glm::rotateY(vect, state.angles.y);
	vect = glm::rotateZ(vect, state.angles.z);
	return state.pos + vect;
}

void LSystem::addNewCoord(State& state, std::vector<float>& coord){
	coord.push_back(state.pos.x);
	coord.push_back(state.pos.z);
	coord.push_back(state.pos.y);
	coord.push_back(state.radius);
}

void LSystem::initState(
		State& state,
		glm::vec3	pos,
		float	stepDistance,
		float radius,
		glm::vec3	angles,
		float	angle,
		float	invert) {

		state.pos = pos;
		state.stepDistance = stepDistance;
		state.radius = radius;
		state.angles = angles;
		state.angle = angle;
		state.invert = invert;
	}
