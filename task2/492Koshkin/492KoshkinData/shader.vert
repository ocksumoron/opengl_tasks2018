#version 330

//стандартные матрицы для преобразования координат
uniform mat4 model_matrix; //из локальной в мировую
uniform mat4 view_matrix; //из мировой в систему координат камеры
uniform mat4 projection_matrix; //из системы координат камеры в усеченные координаты
uniform mat3 normal_to_camera_matrix;

layout(location = 0) in vec3 position; //координаты вершины в локальной системе координат
layout(location = 1) in vec3 normal; //нормаль в локальной системе координат
layout(location = 2) in vec2 tex_coord; //текстурные координаты вершины

out vec3 vertex_normal; //нормаль в системе координат камеры
out vec3 vertex_position; //координаты вершины в системе координат камеры
out vec2 vertex_tex_coord; //текстурные координаты

void main() {
	vertex_tex_coord = tex_coord;
    mat4 model_view = view_matrix * model_matrix;
	vertex_position = (model_view * vec4(position, 1.0)).xyz; //преобразование координат вершины в систему координат камеры
	vertex_normal =  normalize(normal_to_camera_matrix * normal); //преобразование нормали в систему координат камеры
	gl_Position = projection_matrix * model_view * vec4(position, 1.0);
}
