#pragma once

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GL/glew.h>

#include "mesh.hpp"
#include <vector>
#include <iostream>

MeshPtr make_cone(float bottom_width, float top_width, float height);
