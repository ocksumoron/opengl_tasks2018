#define _USE_MATH_DEFINES

#include "l_system.hpp"

#include <cmath>
#include <iostream>
#include <stack>
#include <glm/gtc/matrix_transform.hpp>


LSystem::LSystem(
    const std::map<char, std::string>& rules,
    std::string initial_string,
    float rotate_angle,
    float step_distance,
    float distance_scale,
    float angle_scale,
    float start_width,
    float width_scale
) :
    rules(rules),
    initial_string(initial_string),
    current_string(initial_string),
    initial_rotate_string(rotate_angle * M_PI / 180),
    initial_step_distance(step_distance),
    distance_scale(distance_scale),
    angle_scale(angle_scale),
    start_width(start_width),
    width_scale(width_scale) {
}


void LSystem::execute_iterations(uint16_t num_iterations) {
    for (uint16_t i = 0; i < num_iterations; ++i) {
        std::string newString;
        for (auto ch: current_string) {
            auto rule = rules.find(ch);
            if (rule == rules.end())
                newString += ch;
            else
                newString += rule->second;
        }
        current_string = newString;
    }
}


std::vector<std::pair<glm::vec3, float>> LSystem::get_points() {
    std::vector<std::pair<glm::vec3, float>> result;

    State state;
    std::stack<State> states_stack;
    state.position = glm::vec4(0, 0.0, 0, 1);
    state.angles = glm::vec3(0, 0, 0);
    state.rotate_angle = initial_rotate_string;
    state.step_distance = initial_step_distance;
    state.invert = 1;
    state.width = start_width;

    for (auto ch: current_string) {
        switch (ch) {
            case '[':
                states_stack.push(state);
                break;
            case ']':
                state = states_stack.top();
                states_stack.pop();
                break;
            case 'F':
                push_position(state, result);
                update_state(state);
                push_position(state, result);
                break;
            case 'f':
                update_state(state);
                break;
            case '!':
                state.invert *= -1;
                break;
            case '+':
                state.angles.z += state.invert * state.rotate_angle;
                break;
            case '-':
                state.angles.z -= state.invert * state.rotate_angle;
                break;
            case '&':
                state.angles.x += state.invert * state.rotate_angle;
                break;
            case '^':
                state.angles.x -= state.invert * state.rotate_angle;
                break;
            case '<':
                state.angles.y += state.invert * state.rotate_angle;
                break;
            case '>':
                state.angles.y -= state.invert * state.rotate_angle;
                break;
        }
    }
    return result;
}


glm::mat4 LSystem::get_step(const State& state) {
    glm::mat4 RotateX = glm::rotate(glm::mat4(1), state.angles.x, glm::vec3(1, 0, 0));
    glm::mat4 RotateYX = glm::rotate(RotateX, state.angles.y, glm::vec3(0, 1, 0));
    glm::mat4 RotateZYX= glm::rotate(RotateYX, state.angles.z, glm::vec3(0, 0, 1));
    glm::vec4 dim4_translate = RotateZYX * glm::vec4(0, state.step_distance, 0, 0);
    glm::vec3 translate(dim4_translate.x, dim4_translate.y, dim4_translate.z);
    glm::mat4 StepTransformation = glm::translate(glm::mat4(1), translate);
    return StepTransformation;
}


void LSystem::push_position(const State& state, std::vector<std::pair<glm::vec3, float>>& result) {
    result.push_back(std::make_pair(
        glm::vec3(
            state.position.x,
            state.position.z,
            state.position.y
        ),
        state.width
    ));
}


void LSystem::update_state(State& state) {
    glm::mat4 StepTransformation = get_step(state);
    state.position = StepTransformation * state.position;
    state.step_distance *= distance_scale;
    state.rotate_angle *= angle_scale;
    state.width *= width_scale;
}
