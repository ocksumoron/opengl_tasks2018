#include "tree_application.cpp"

int main() {
    TreeApplication app;
    std::map<char, std::string> rules;
    rules['A'] = "[F>[+FA][>FB][<FB][-FA]<]";
    rules['B'] = "[F<[>FB][+FA][-FA][<FB]>]";
    std::string initial_string = "AB";
    float rotate_angle = 60;
    float step_distance = 0.5;
    float distance_scale = 0.7;
    float angle_scale = 0.9;
    float start_width = 0.3;
    float width_scale = 0.6;
    int num_iterations = 5;

    app.setup_LSystem(
        rules,
        initial_string,
        rotate_angle,
        step_distance,
        distance_scale,
        angle_scale,
        start_width,
        width_scale,
        num_iterations
    );
    app.start();

	return 0;
}
