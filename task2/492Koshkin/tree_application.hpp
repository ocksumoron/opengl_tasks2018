#pragma once

#include "application.hpp"
#include "mesh.hpp"
#include "shader_program.hpp"
#include "texture.hpp"
#include "light_info.hpp"

#include <cmath>
#include <iostream>
#include <vector>

#include "cone.hpp"
#include "leaf.hpp"
#include "l_system.hpp"

#include <ctime>
#include <cstdlib>

#define GLM_SWIZZLE_XYZ

class TreeApplication : public Application {
private:
    LightInfo light;
    std::vector<MeshPtr> branches;
    std::vector<glm::mat4> leafs_model_matrixes;
    MeshPtr leaf;
    ShaderProgramPtr tree_shader;
    ShaderProgramPtr leaf_shader;
    LSystem system;
    std::vector<std::pair<glm::vec3, float>> points;

    TexturePtr wood_texture;
    TexturePtr normal_map_texture;
    TexturePtr leaf_texture;

    GLuint sampler;
public:
    void setup_LSystem(
        std::map<char, std::string> rules,
        std::string initialString,
        float rotate_angle,
        float step_distance,
        float distance_scale,
        float angle_scale,
        float start_width,
        float width_scale,
        int num_iterations
    ) {
        system = LSystem(
            rules, initialString,
            rotate_angle, step_distance,
            distance_scale, angle_scale,
            start_width, width_scale
        );
        system.execute_iterations(num_iterations);
        points = system.get_points();
    }


    void add_leaf(const glm::vec3 first_point,
                  const glm::vec3 second_point,
                  const float bottom_width,
                  const float top_width) {
        //Создаем меш с цилиндром
        float distance = glm::distance(first_point, second_point);
        glm::vec3 projection(second_point[0], second_point[1], first_point[2]);

        float position_scale = (float)std::rand() / RAND_MAX;
        glm::vec3 leaf_start = first_point + position_scale * (second_point - first_point);
        glm::mat4 model_matrix = glm::translate(glm::mat4(1.0f), leaf_start);
        if (projection != first_point) {
            float sin = glm::distance(projection, first_point) / distance;
            float angle = std::asin(sin);
            glm::vec3 axis = glm::cross(glm::vec3(0, 0, 1), projection - first_point);
            model_matrix = glm::rotate(model_matrix, angle, axis);
        }

        float angle = (float)std::rand() / RAND_MAX * 2 * M_PI;
        model_matrix = glm::rotate(model_matrix, angle, glm::vec3(0.0, 0.0, 1.0));

        float skip = (bottom_width + position_scale * (top_width - bottom_width)) / 2;
        model_matrix = glm::translate(model_matrix, glm::vec3(skip, 0.0, 0.0));
        leafs_model_matrixes.push_back(model_matrix);
    }

    void make_branch(glm::vec3 first_point,
                     glm::vec3 second_point,
                     float bottom_width,
                     float top_width,
                     const int leafs_number) {
        if (second_point[2] < first_point[2]) {
            glm::vec3 tmp_point = first_point;
            first_point = second_point;
            second_point = tmp_point;
            float tmp_width = top_width;
            top_width = bottom_width;
            bottom_width = tmp_width;
        }
        //Создаем меш с цилиндром
        float distance = glm::distance(first_point, second_point);
        glm::vec3 projection(second_point[0], second_point[1], first_point[2]);
        glm::mat4 model_matrix;
        glm::mat4 translation = glm::translate(glm::mat4(1.0f), first_point);
        if (projection != first_point) {
            float sin = glm::distance(projection, first_point) / distance;
            float angle = std::asin(sin);
            glm::vec3 axis = glm::cross(glm::vec3(0, 0, 1), projection - first_point);
            model_matrix = glm::rotate(translation, angle, axis);
        } else {
            model_matrix = translation;
        }

        MeshPtr cone = make_cone(bottom_width, top_width, distance);
        cone->setModelMatrix(model_matrix);
        branches.push_back(cone);

        for (int i = 0; i < leafs_number; ++i) {
            add_leaf(first_point, second_point, bottom_width, top_width);
        }
    }

    void makeScene() override {
        std::srand(unsigned(std::time(0)));
        Application::makeScene();

        float size = 0.05;
        leaf = make_leaf(size, size / 2, size / 16);

        _cameraMover = std::make_shared<FreeCameraMover>();
        glm::vec3 root_position(0.0f, 0.0f, 0.0f);
        for (auto it = points.begin(); it != points.end(); ++it) {
            // Сейчас тут надо перепутать y и z, не понятно почему.
            glm::vec3 start_point = it->first + root_position;
            float bottom_width = it->second;
            ++it;
            glm::vec3 end_point = it->first + root_position;
            float top_width = it->second;

            if (top_width < 0.05) {
                make_branch(start_point, end_point, bottom_width, top_width, 3);
            } else {
                make_branch(start_point, end_point, bottom_width, top_width, 0);
            }
        }

        // Создаем шейдерную программу        
        tree_shader = std::make_shared<ShaderProgram>("492KoshkinData/shader.vert", "492KoshkinData/tree.frag");
        leaf_shader = std::make_shared<ShaderProgram>("492KoshkinData/shader.vert", "492KoshkinData/leaf.frag");


        // Загрузка и создание текстур
        wood_texture = loadTexture("492KoshkinData/wood_texture.jpg");
        normal_map_texture = loadTexture("492KoshkinData/wood_normal_map.jpg");
        leaf_texture = loadTexture("492KoshkinData/leaf_rgba.png");
        //wood_texture = loadTexture("492KoshkinData/bicolor.jpg");

        // Инициализация сэмплеров
        glGenSamplers(1, &sampler);
        glSamplerParameteri(sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glSamplerParameteri(sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glSamplerParameterf(sampler, GL_TEXTURE_MAX_ANISOTROPY_EXT, 10);
    }

    void draw() override {
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);
        glViewport(0, 0, width, height);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Устанавливаем шейдер.
        tree_shader->use();

        //Устанавливаем общие юниформ-переменные
        tree_shader->setMat4Uniform("view_matrix", _camera.viewMatrix);
        tree_shader->setMat4Uniform("projection_matrix", _camera.projMatrix);

        // Инициализация значений переменных освщения
        light.direction = glm::vec3(0.0, 0.0, -1.0);
        light.ambient = glm::vec3(0.2, 0.2, 0.2);
        light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        light.specular = glm::vec3(1.0, 1.0, 1.0);
        tree_shader->setMat3Uniform("light_dir_to_cam_space", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix))));
        tree_shader->setVec3Uniform("light.direction", light.direction);
        tree_shader->setVec3Uniform("light.La", light.ambient);
        tree_shader->setVec3Uniform("light.Ld", light.diffuse);
        tree_shader->setVec3Uniform("light.Ls", light.specular);

        GLuint texture_unit = 0;
        glBindTextureUnit(texture_unit, wood_texture->texture());
        glBindSampler(texture_unit, sampler);
        tree_shader->setIntUniform("diffuse_tex", texture_unit);

        GLuint normal_map_unit = 1;
        glBindTextureUnit(normal_map_unit, normal_map_texture->texture());
        glBindSampler(normal_map_unit, sampler);
        tree_shader->setIntUniform("normal_map_tex", normal_map_unit);

        glEnable(GL_BLEND);
        glBlendFunc(GL_ONE, GL_ZERO);

        // Рисуем ветки
        for (auto branch: branches) {
            tree_shader->setMat4Uniform("model_matrix", branch->modelMatrix());
            tree_shader->setMat3Uniform("normal_to_camera_matrix", glm::transpose(glm::inverse(glm::mat3(
                _camera.viewMatrix * branch->modelMatrix()))
            ));
            branch->draw();
        }

        //Устанавливаем шейдер.
        leaf_shader->use();

        //Устанавливаем общие юниформ-переменные
        leaf_shader->setMat4Uniform("view_matrix", _camera.viewMatrix);
        leaf_shader->setMat4Uniform("projection_matrix", _camera.projMatrix);

        // Инициализация значений переменных освщения
        light.specular = glm::vec3(0.2, 0.2, 0.2);
        leaf_shader->setMat3Uniform("light_dir_to_cam_space", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix))));
        leaf_shader->setVec3Uniform("light.direction", light.direction);
        leaf_shader->setVec3Uniform("light.La", light.ambient);
        leaf_shader->setVec3Uniform("light.Ld", light.diffuse);
        leaf_shader->setVec3Uniform("light.Ls", light.specular);

        GLuint leaf_unit = 2;
        glBindTextureUnit(leaf_unit, leaf_texture->texture());
        glBindSampler(leaf_unit, sampler);
        leaf_shader->setIntUniform("diffuse_tex", leaf_unit);

        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        //Рисуем листья
        for (auto model_matrix: leafs_model_matrixes) {
            leaf_shader->setMat4Uniform("model_matrix", model_matrix);
            leaf_shader->setMat3Uniform("normal_to_camera_matrix", glm::transpose(glm::inverse(glm::mat3(
                _camera.viewMatrix * model_matrix))
            ));
            leaf->draw();
        }

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);
    }
};
