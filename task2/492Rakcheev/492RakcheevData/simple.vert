#version 330

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat3 normalMatrix;

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;
layout(location = 2) in vec2 vertexTexCoord; //текстурные координаты вершины

out vec3 normalCamSpace; //нормаль в системе координат камеры
out vec3 posCamSpace; //координаты вершины в системе координат камеры
out vec2 texCoord; //текстурные координаты

void main() {
    mat4 modelTmp = viewMatrix * modelMatrix;
    texCoord = vertexTexCoord;    
    posCamSpace = (modelTmp * vec4(vertexPosition, 1.0)).xyz; //преобразование координат вершины в систему координат камеры
    normalCamSpace = normalMatrix * vertexNormal;

    gl_Position = projectionMatrix * modelTmp * vec4(vertexPosition, 1.0);
}
