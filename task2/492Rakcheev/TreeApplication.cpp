#include "TreeApplication.h"
#include <iostream>
#include <cstdlib>
#include <ctime>

void TreeApplication::setupLSystem(
    std::map<char, std::string> rules,
    std::string initialString,
    float rotateAngle,
    float stepDistance,
    float distanceScale,
    float angleScale,
    float radiusScale,
    float initialBottomRadius,
    uint16_t numIterations
) {
    system = LSystem(rules, initialString, rotateAngle, stepDistance, distanceScale, angleScale, radiusScale, initialBottomRadius);
    system.executeIterations(numIterations);
    points = system.getPoints();
}


void TreeApplication::createLeaf(glm::vec3 firstPoint, glm::vec3 secondPoint, float bottomWidth, float topWidth) {
        float distance = glm::distance(firstPoint, secondPoint);
        glm::vec3 projection(secondPoint[0], secondPoint[1], firstPoint[2]);

        float positionScale = (float)std::rand() / RAND_MAX;
        glm::vec3 leafStart = firstPoint + positionScale * (secondPoint - firstPoint);
        glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), leafStart);
        if (projection != firstPoint) {
            float sin = glm::distance(projection, firstPoint) / distance;
            float angle = std::asin(sin);
            glm::vec3 axis = glm::cross(glm::vec3(0, 0, 1), secondPoint - firstPoint);
            modelMatrix = glm::rotate(modelMatrix, angle, axis);
        }

        float angle = (float)std::rand() / RAND_MAX * 2 * M_PI;
        modelMatrix = glm::rotate(modelMatrix, angle, glm::vec3(0.0, 0.0, 1.0));
        float size = 0.04;
        float skip = (bottomWidth + positionScale * (topWidth - bottomWidth)) / 2;
        modelMatrix = glm::translate(modelMatrix, glm::vec3(skip, 0.0, 0.0));
        //MeshPtr leaf = makeLeaf(skip, size, size / 2, size / 25);
        //leaf->setModelMatrix(modelMatrix);
        //crown.push_back(leaf);
        crown.push_back(modelMatrix);
    }


void TreeApplication::makeBranch(glm::vec3 firstPoint, glm::vec3 secondPoint, float bottomRadius, float topRadius, int lfnum) {
    float distance = glm::distance(firstPoint, secondPoint);
    if (firstPoint[2] > secondPoint[2]) {
        glm::vec3 tmp = secondPoint;
        secondPoint = firstPoint;
        firstPoint = tmp;
        float tmpRadius = topRadius;
        topRadius = bottomRadius;
        bottomRadius = tmpRadius;
    }
    glm::vec3 projection(secondPoint[0], secondPoint[1], firstPoint[2]);
    glm::mat4 modelMatrix;
    glm::mat4 translation = glm::translate(glm::mat4(1.0f), firstPoint);
    if (projection != firstPoint) {
        float sin = glm::distance(projection, firstPoint) / distance;
        float angle = std::asin(sin);
        glm::vec3 axis = glm::cross(glm::vec3(0, 0, 1), secondPoint - firstPoint);
        modelMatrix = glm::rotate(translation, angle, axis);
    } else {
        modelMatrix = translation;
    }

    MeshPtr cone = makeCone(bottomRadius, topRadius, distance, firstPoint, secondPoint);
    cone->setModelMatrix(modelMatrix);
    branches.push_back(cone);

    for (int i = 0; i < lfnum; i++) {
        createLeaf(glm::vec3(firstPoint[0], firstPoint[1], firstPoint[2]), 
                   glm::vec3(secondPoint[0], secondPoint[1], secondPoint[2]), 
                   bottomRadius, topRadius);
    }
}


void TreeApplication::makeScene() {
    Application::makeScene();
    float size = 0.04;
    leaf = makeLeaf(size, size / 2, size / 25);

    _cameraMover = std::make_shared<FreeCameraMover>();

    for (auto it = points.begin(); it != points.end(); ++it) {
        int lfnum = 0;
        glm::vec3 firstPoint = it->first;
        float bottomRadius = it->second;
        ++it;
        glm::vec3 secondPoint = it->first;
        float topRadius = it->second;
        if (topRadius < 0.016) {
            lfnum = 6;
        }
        makeBranch(
            firstPoint,
            secondPoint,
            bottomRadius, topRadius, lfnum
        );
    }

    //Создаем шейдерную программу        
    shader = std::make_shared<ShaderProgram>("492RakcheevData/simple.vert", "492RakcheevData/simple.frag");
    lfShader = std::make_shared<ShaderProgram>("492RakcheevData/simple.vert", "492RakcheevData/leaf.frag");
    
    // Инициализация значений переменных освщения
    light.direction = glm::vec3(0.0, 0.0, -1.0);
    light.ambient = glm::vec3(0.2, 0.2, 0.2);
    light.diffuse = glm::vec3(0.8, 0.8, 0.8);
    light.specular = glm::vec3(1.0, 1.0, 1.0);

    //=========================================================
    //Загрузка и создание текстур
    barkTexture = loadTexture("492RakcheevData/bark_2.jpg");
    leafTexture = loadTexture("492RakcheevData/leaf.png");

    //=========================================================
    //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
    glGenSamplers(1, &sampler);
    glSamplerParameteri(sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glSamplerParameteri(sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glSamplerParameteri(sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glSamplerParameteri(sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glSamplerParameterf(sampler, GL_TEXTURE_MAX_ANISOTROPY_EXT, 5);

    glGenSamplers(1, &lfSampler);
    glSamplerParameteri(lfSampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glSamplerParameteri(lfSampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glSamplerParameteri(lfSampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glSamplerParameteri(lfSampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
}

void TreeApplication::draw() {
    Application::draw();

    //Получаем размеры экрана (окна)
    int width, height;
    glfwGetFramebufferSize(_window, &width, &height);

    //Устанавливаем порт вывода на весь экран (окно)
    glViewport(0, 0, width, height);

    //Очищаем порт вывода (буфер цвета и буфер глубины)
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //Подключаем шейдерную программу
    shader->use();

    //Загружаем на видеокарту значения юниформ-переменных
    shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
    shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
    
    /*
    glm::mat4 model_matrix = glm::translate(glm::mat4(1.0f), glm::vec3(0.0, 0.0, 10.0));
    glm::vec3 lightPos = glm::vec3(_camera.viewMatrix * model_matrix * glm::vec4(light.position, 1.0));
    */
    

    shader->setMat3Uniform("lightDirCamSpace", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix))));
    shader->setVec3Uniform("light.dir", light.direction);
    shader->setVec3Uniform("light.La", light.ambient);
    shader->setVec3Uniform("light.Ld", light.diffuse);
    shader->setVec3Uniform("light.Ls", light.specular);

    // glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
    glBindTextureUnit(0, barkTexture->texture());
    glBindSampler(0, sampler);
    // barkTexture->bind();
    shader->setIntUniform("diffuseTex", 0);


    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ZERO);

    //Рисуем мешы
    for (auto branch: branches) {
        shader->setMat4Uniform("modelMatrix", branch->modelMatrix());
        shader->setMat3Uniform("normalMatrix", glm::transpose(glm::inverse(glm::mat3(
                _camera.viewMatrix * branch->modelMatrix()))));
        branch->draw();
    }

    lfShader->use();

    lfShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
    lfShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);


    lfShader->setMat3Uniform("lightDirCamSpace", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix))));
    lfShader->setVec3Uniform("light.dir", light.direction);
    lfShader->setVec3Uniform("light.La", light.ambient);
    lfShader->setVec3Uniform("light.Ld", light.diffuse);
    lfShader->setVec3Uniform("light.Ls", light.specular);

    glBindTextureUnit(1, leafTexture->texture());
    glBindSampler(1, lfSampler);
    lfShader->setIntUniform("diffuseTex", 1);

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    for (auto curLeaf: crown) {
        lfShader->setMat4Uniform("modelMatrix", curLeaf);
        lfShader->setMat3Uniform("normalMatrix", glm::transpose(glm::inverse(glm::mat3(
                                    _camera.viewMatrix * curLeaf))));
        leaf->draw();
    }


    glBindSampler(0, 0);
    glUseProgram(0);
}
