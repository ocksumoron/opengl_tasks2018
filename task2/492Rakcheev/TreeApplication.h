#include <Application.hpp>
#include "Mesh.hpp"
#include <ShaderProgram.hpp>
#include "LightInfo.hpp"
#include "Texture.hpp"

#include <iostream>
#include <utility>
#include <vector>
#include <Common.h>

#include "LSystem.h"


class TreeApplication : public Application {
    std::vector<MeshPtr> branches;
    std::vector<glm::mat4> crown;
    ShaderProgramPtr shader;
    ShaderProgramPtr lfShader;
    LSystem system;
    MeshPtr leaf;
    std::vector<std::pair<glm::vec3, float>> points;
    LightInfo light;
    TexturePtr barkTexture;
    TexturePtr leafTexture;
    GLuint sampler;
    GLuint lfSampler;
public:
    void setupLSystem(
        std::map<char, std::string> rules,
        std::string initialString,
        float rotateAngle,
        float stepDistance,
        float distanceScale,
        float angleScale,
        float radiusScale,
        float initialBottomRadius,
        uint16_t numIterations
    );
    void createLeaf(glm::vec3 firstPoint, glm::vec3 secondPoint, float bottomWight, float topWight);
    void makeBranch(glm::vec3 firstPoint, glm::vec3 secondPoint, float bottomRadius, float topRadius, int lfnum);
    void makeScene() override;
    void draw() override;
};
