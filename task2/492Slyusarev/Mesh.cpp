#include "Mesh.h"

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <iostream>
#include <vector>


MeshPtr makeSphere(float radius, unsigned int N)
{
	unsigned int M = N / 2;

	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec2> texcoords;

	for (unsigned int i = 0; i < M; i++)
	{
		float theta = (float)glm::pi<float>() * i / M;
		float theta1 = (float)glm::pi<float>() * (i + 1) / M;

		for (unsigned int j = 0; j < N; j++)
		{
			float phi = 2.0f * (float)glm::pi<float>() * j / N + (float)glm::pi<float>();
			float phi1 = 2.0f * (float)glm::pi<float>() * (j + 1) / N + (float)glm::pi<float>();

			//������ �����������, ���������� ����
			vertices.push_back(glm::vec3(cos(phi) * sin(theta) * radius, sin(phi) * sin(theta) * radius, cos(theta) * radius));
			vertices.push_back(glm::vec3(cos(phi1) * sin(theta1) * radius, sin(phi1) * sin(theta1) * radius, cos(theta1) * radius));
			vertices.push_back(glm::vec3(cos(phi1) * sin(theta) * radius, sin(phi1) * sin(theta) * radius, cos(theta) * radius));

			normals.push_back(glm::vec3(cos(phi) * sin(theta), sin(phi) * sin(theta), cos(theta)));
			normals.push_back(glm::vec3(cos(phi1) * sin(theta1), sin(phi1) * sin(theta1), cos(theta1)));
			normals.push_back(glm::vec3(cos(phi1) * sin(theta), sin(phi1) * sin(theta), cos(theta)));

			texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)i / M));
			texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)(i + 1) / M));
			texcoords.push_back(glm::vec2((float)j/ N, 1.0f - (float)(i+1) / M));

			//������ �����������, ���������� ����
			vertices.push_back(glm::vec3(cos(phi) * sin(theta) * radius, sin(phi) * sin(theta) * radius, cos(theta) * radius));
			vertices.push_back(glm::vec3(cos(phi) * sin(theta1) * radius, sin(phi) * sin(theta1) * radius, cos(theta1) * radius));
			vertices.push_back(glm::vec3(cos(phi1) * sin(theta1) * radius, sin(phi1) * sin(theta1) * radius, cos(theta1) * radius));

			normals.push_back(glm::vec3(cos(phi) * sin(theta), sin(phi) * sin(theta), cos(theta)));
			normals.push_back(glm::vec3(cos(phi) * sin(theta1), sin(phi) * sin(theta1), cos(theta1)));
			normals.push_back(glm::vec3(cos(phi1) * sin(theta1), sin(phi1) * sin(theta1), cos(theta1)));

			texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)i / M));
			texcoords.push_back(glm::vec2((float)(j+1) / N, 1.0f - (float)i / M));
			texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)(i + 1) / M));
		}
	}

	//----------------------------------------

	DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

	DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

	DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

	MeshPtr mesh = std::make_shared<Mesh>();
	mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
	mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
	mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
	mesh->setPrimitiveType(GL_TRIANGLES);
	mesh->setVertexCount(vertices.size());

	std::cout << "Sphere is created with " << vertices.size() << " vertices\n";

	return mesh;
}


glm::vec3 getMoebiusStripVertice(float radius, float u, float v) {
	float x = radius * (cos(v) + u * cos(v / 2) * cos(v));
	float y = radius * (sin(v) + u * cos(v / 2) * sin(v));
	float z = radius * u * sin(v / 2);
	return glm::vec3(x, y, z);
}

inline float squared(float number) {
	return number * number;
}

 inline float norm(float x, float y, float z) {
	return sqrt(squared(x) + squared(y) + squared(z));
}

glm::vec3 getMoebiusStripNormal(float radius, float u, float v) {
	float nx = -0.5f * squared(radius) * (-u * cos(v / 2.f) + 2.f * cos(v) + u * cos((3.f * v) / 2.f)) * sin(v / 2.f);
	float ny = -0.25f * squared(radius) * (2.f * u * cos(v) - u * cos(2.f * v) + u + 2.f * cos(v / 2.f) - 2.f * cos((3.f * v) / 2.f));
	float nz = squared(radius) * cos(v / 2.f) * (u * cos(v / 2.f) + 1.f);
	float n0 = norm(nx, ny, nz);
	return glm::vec3(nx / n0, ny / n0, nz / n0);
}

MeshPtr makeMoebiusStrip(float radius, unsigned int N)
{
	unsigned int M = N / 2;

	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec2> texcoords;

	float umin = -1.f;
	float umax = 1.f;

	for (unsigned int i = 0; i < M; i++)
	{
		float u =  umin + (umax - umin) * i / M;
		float u1 = umin + (umax - umin) * (i + 1) / M;

		for (unsigned int j = 0; j < N; j++)
		{
			float v = 2.0f * (float)glm::pi<float>() * j / N;
			float v1 = 2.0f * (float)glm::pi<float>() * (j + 1) / N;

			//������ �����������, ���������� ����
			vertices.push_back(getMoebiusStripVertice(radius, u, v));
			vertices.push_back(getMoebiusStripVertice(radius, u1, v1));
			vertices.push_back(getMoebiusStripVertice(radius, u1, v));

			normals.push_back(getMoebiusStripNormal(radius, u, v));
			normals.push_back(getMoebiusStripNormal(radius, u1, v1));
			normals.push_back(getMoebiusStripNormal(radius, u1, v));

			texcoords.push_back(glm::vec2((float)j / N, (float)i / M));
			texcoords.push_back(glm::vec2((float)(j + 1) / N, (float)(i + 1) / M));
			texcoords.push_back(glm::vec2((float)j / N, (float)(i+1) / M));

			//������ �����������, ���������� ����
			vertices.push_back(getMoebiusStripVertice(radius, u, v));
			vertices.push_back(getMoebiusStripVertice(radius, u, v1));
			vertices.push_back(getMoebiusStripVertice(radius, u1, v1));

			normals.push_back(getMoebiusStripNormal(radius, u, v));
			normals.push_back(getMoebiusStripNormal(radius, u, v1));
			normals.push_back(getMoebiusStripNormal(radius, u1, v1));

			texcoords.push_back(glm::vec2((float)j / N, (float)i / M));
			texcoords.push_back(glm::vec2((float)(j+1) / N, (float)i / M));
			texcoords.push_back(glm::vec2((float)(j + 1) / N, (float)(i+1) / M));
		}
	}

	//----------------------------------------

	DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

	DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

	DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

	MeshPtr mesh = std::make_shared<Mesh>();
	mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
	mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
	mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);

	mesh->setPrimitiveType(GL_TRIANGLES);
	mesh->setVertexCount(vertices.size());

	std::cout << "Moebius Strip is created with " << vertices.size() << " vertices\n";

	return mesh;
}
