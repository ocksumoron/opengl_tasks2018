/**
Использование текстуры в качестве коэффициента отражения света
*/

#version 330

uniform sampler2D diffuseTexture;
uniform sampler2D normalMapTexture;
uniform mat4 viewMatrix;

struct LightInfo
{
    //направление на источник света в мировой системе координат (для направленного источника)
    vec3 dir;
    //цвет и интенсивность окружающего света
    vec3 La;
    //цвет и интенсивность диффузного света
    vec3 Ld;
    //цвет и интенсивность бликого света
    vec3 Ls;
};

uniform LightInfo light;

struct MaterialInfo
{
    //коэффициент отражения окружающего света
    vec3 Ka;
    //коэффициент отражения диффузного света
    vec3 Kd;
    //коэффициент отражения бликого света
    vec3 Ks;
};
uniform MaterialInfo material;


//нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec3 normal;

//координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec3 position;

//текстурные координаты (интерполирована между вершинами треугольника)
in vec2 textureCoords;

//выходной цвет фрагмента
out vec4 fragColor;

const float shininess = 128.0;

// http://www.thetenthplanet.de/archives/1180 - link to normal mapping realization
mat3 cotangentFrame(vec3 normal, vec3 pos, vec2 uv) {
    vec3 dp1 = dFdx(pos);
    vec3 dp2 = dFdy(pos);
    vec2 duv1 = dFdx(uv);
    vec2 duv2 = dFdy(uv);
    vec3 dp2perp = cross(dp2, normal);
    vec3 dp1perp = cross(normal, dp1);
    vec3 T = dp2perp * duv1.x + dp1perp * duv2.x;
    vec3 B = dp2perp * duv1.y + dp1perp * duv2.y;
    float invmax = inversesqrt(max(dot(T, T), dot(B, B)));
    return mat3(T * invmax, B * invmax, normal);
}

void main()
{
	vec3 diffuseColor = texture(diffuseTexture, textureCoords).rgb;
	vec3 normalMap = texture(normalMapTexture, textureCoords).rgb;

    //нормализуем нормаль после интерполяции
	vec3 normedNormal = normalize(normal);

    //направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))
	vec3 viewDirection = normalize(-position.xyz);

    //направление на источник света - из мировой в систему координат камеры
    vec4 lightDirCamSpace = viewMatrix * normalize(vec4(light.dir, 0.0));
    mat3 tbn = cotangentFrame(normedNormal, position, textureCoords);
    normedNormal = normalize(tbn * (normalMap * 2.0 - 1.0));

    //скалярное произведение (косинус)
	float NdotL = max(dot(normedNormal, lightDirCamSpace.xyz), 0.0);

	vec3 color = diffuseColor * (light.La * material.Ka + light.Ld * material.Kd * NdotL);

	if (NdotL > 0.0)
	{
		//биссектриса между направлениями на камеру и на источник света
        vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection);

		//интенсивность бликового освещения по Блинну
        float blinnTerm = max(dot(normedNormal, halfVector), 0.0);

		//регулируем размер блика
        blinnTerm = pow(blinnTerm, shininess);
		color += light.Ls * material.Ks * blinnTerm;
	}

	fragColor = vec4(color, 1.0);
}
