#pragma once

#include <map>
#include <vector>
#include <string>
#include <utility>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>

#include "Cone.h"


struct State {
    glm::vec3 angles;
    glm::vec4 position;
    float stepDistance;
    float rotateAngle;
    float thickness;
    float invert;
};


class LSystem {
    float initialRotateAngle;
    float initialStepDistance;
    float initialThickness;
    float distanceScale;
    float angleScale;
    float thicknessScale;
    std::map<char, std::string> rules;
    std::string initialString;
    std::string currentString;
public:
    LSystem() = default;
    LSystem(
        const std::map<char, std::string>& rules,
        std::string initialString,
        float stepDistance,
        float rotateAngle,
        float thickness,
        float distanceScale,
        float angleScale,
        float thicknessScale
    );
    void executeIterations(uint16_t numIterations);
    std::vector<ConeParams> getConesParams();
protected:
    glm::mat4 getStep(const State& state);
    glm::vec3 getPoint(const State& state);
    void UpdateState(State& state);
};
