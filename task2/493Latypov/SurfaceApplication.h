#include "common/ShaderProgram.hpp"
#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/PerlinNoise.hpp"
#include "common/Texture.hpp"
#include "common/LightInfo.hpp"

#include <iostream>
#include <vector>

class SurfaceApplication : public Application {
public:
    SurfaceApplication();

    void makeScene() override;

    void draw() override;

    void updateGUI() override;

protected:
    TerrainPtr mesh_;
    ShaderProgramPtr shader_program_;

    float phi_ = 0.0f;
    float theta_ = glm::pi<float>() * 0.25f;

    TexturePtr snow_, grass_, sand_, stone_, map_, specular_;
    GLuint sampler_;
    LightInfo light_info_;
};
