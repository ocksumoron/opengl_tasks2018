#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <memory>
#include <utility>
#include "Mesh.hpp"

#define GLM_FORCE_RADIANS

struct CameraInfo {
    glm::mat4 view_matrix;
    glm::mat4 projection_matrix;
};

// Класс для управления виртуальной камерой
class CameraHandler {
public:
    CameraHandler() = default;

    virtual ~CameraHandler() = default;

    // Возвращает параметры виртуальной камеры в виде 2х матриц: матрицы вида и проекции
    CameraInfo GetCameraInfo() const { return camera_info_; }

    // Обрабатывает нажатия кнопок на клавитуре. См. сигнатуру GLFWkeyfun библиотеки GLFW
    virtual void handleKey(GLFWwindow *window, int key, int scancode, int action, int mods) = 0;

    // Обрабатывает движение мышки
    virtual void handleMouseMove(GLFWwindow *window, double xpos, double ypos) = 0;

    // Обрабатывает колесико мыши
    virtual void handleScroll(GLFWwindow *window, double xoffset, double yoffset) = 0;

    // Обновляет положение виртуальной камеры
    virtual void update(GLFWwindow *window, double dt) = 0;

protected:
    CameraInfo camera_info_;
};

class FreeCameraHandler : public CameraHandler {
public:
    FreeCameraHandler();

    void handleKey(GLFWwindow *window, int key, int scan_code, int action, int mods) override;

    void handleMouseMove(GLFWwindow *window, double x_pos, double y_pos) override;

    void handleScroll(GLFWwindow *window, double x_offset, double y_offset) override;

    void update(GLFWwindow *window, double dt) override;

protected:
    // Положение виртуальный камеры задается в сферических координат
    glm::vec3 pos_;
    glm::quat rot_;

    // Положение курсора мыши на предыдущем кадре
    double old_x_pos_ = 0.0;
    double old_y_pos_ = 0.0;
};

class FirstPersonCameraHandler : public FreeCameraHandler {
public:
    explicit FirstPersonCameraHandler(TerrainPtr mesh) : mesh_(std::move(mesh)) {
        is_first_person_ = false;
    }

    void update(GLFWwindow *window, double dt) override;

protected:
    TerrainPtr mesh_;
    bool is_first_person_;
};

