#include "Mesh.hpp"
#include "PerlinNoise.hpp"

#include <iostream>

typedef std::vector<std::vector<std::vector<std::pair<int, glm::vec3>>>> NormalsData;

TerrainPtr calculateSurface(float size, unsigned int frequency, int num_octaves, float persistence) {
    std::vector<glm::vec3> vertices;

    NormalsData normals_ij(frequency + 2,
                           std::vector<std::vector<std::pair<int, glm::vec3>>>(frequency + 2,
                                                                               std::vector<std::pair<int, glm::vec3>>()));

    std::vector<glm::vec2> tex_coordinates;
    std::vector<glm::vec2> map_coordinates;

    PerlinNoise noise = PerlinNoise(num_octaves, persistence);

    float step = 2.f / frequency;
    float quality = 10.f;

    // Заполняем normals_ij
    int it = 0;
    int ii = 0;
    int jj = 0;
    for (auto i = -1.0F; i < 1.0F; i += step, ++ii) {
        jj = 0;
        for (auto j = -1.0F; j < 1.0F; j += step, ++jj) {
            // Первый треугольник, first, second, third - его вершины
            // x, y получаются из i, j (+ step), z из perlinNoise2D
            glm::vec3 first = glm::vec3(i * size, j * size, noise.perlinNoise2D(i, j));
            glm::vec3 second = glm::vec3((i + step) * size, j * size, noise.perlinNoise2D(i + step, j));
            glm::vec3 third = glm::vec3(i * size, (j + step) * size, noise.perlinNoise2D(i, j + step));
            // Добавляем координаты треугольника в общий список вершин
            vertices.push_back(first);
            vertices.push_back(second);
            vertices.push_back(third);

            // Вычисляем нормали к данному треугольнику
            // glm::cross - векторное произведение
            // glm::normalize - нормализация длины полученного перпендикуляра
            glm::vec3 normal = glm::normalize(glm::cross(third - first, second - first));
            // Данные нормали дальше будут интерполироваться, поэтому пока храним их в normals_ij
            normals_ij[ii][jj].emplace_back(it++, -normal);
            normals_ij[ii + 1][jj].emplace_back(it++, -normal);
            normals_ij[ii][jj + 1].emplace_back(it++, -normal);

            float tx = (i + 1.f) * quality;
            float ty = (j + 1.f) * quality;
            tex_coordinates.emplace_back(tx, ty);
            tex_coordinates.emplace_back(tx + step * quality, ty);
            tex_coordinates.emplace_back(tx, ty + step * quality);

            // Coordinates for map texture
            float x = (i + 1.f) / 2.f;
            float y = (j + 1.f) / 2.f;
            map_coordinates.emplace_back(x, y);
            map_coordinates.emplace_back(x + step / 2.f, y);
            map_coordinates.emplace_back(x, y + step / 2.f);

            // Аналогично рассматриваем второй треугольник
            first = glm::vec3(i * size, (j + step) * size, noise.perlinNoise2D(i, j + step));
            second = glm::vec3((i + step) * size, (j + step) * size, noise.perlinNoise2D(i + step, j + step));
            third = glm::vec3((i + step) * size, j * size, noise.perlinNoise2D(i + step, j));
            vertices.emplace_back(first);
            vertices.emplace_back(second);
            vertices.emplace_back(third);

            normal = glm::normalize(glm::cross(third - first, second - first));
            normals_ij[ii][jj + 1].emplace_back(it++, normal);
            normals_ij[ii + 1][jj + 1].emplace_back(it++, normal);
            normals_ij[ii + 1][jj].emplace_back(it++, normal);

            tex_coordinates.emplace_back(tx, ty + step * quality);
            tex_coordinates.emplace_back(tx + step * quality, ty + step * quality);
            tex_coordinates.emplace_back(tx + step * quality, ty);

            map_coordinates.emplace_back(x, y + step / 2.f);
            map_coordinates.emplace_back(x + step / 2.f, y + step / 2.f);
            map_coordinates.emplace_back(x + step / 2.f, y);
        }
    }

    // Вычисление интерполированных нормалей
    // Нормалей у нас будет it штук, поскольку
    auto normals = std::vector<glm::vec3>(static_cast<unsigned long>(it));
    for (ii = 0; ii < frequency + 1; ++ii) {
        for (jj = 0; jj < frequency + 1; ++jj) {
            glm::vec3 normal;
            for (auto &elem : normals_ij[ii][jj]) {
                normal += elem.second;
            }
            normal /= normals_ij[ii][jj].size();
            for (auto &elem : normals_ij[ii][jj]) {
                normals[elem.first] = normal;
            }
        }
    }

    // Создаем буфер для данных о координатах вершин
    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    // Создаем буфер для данных о нормалях
    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(tex_coordinates.size() * sizeof(float) * 2, tex_coordinates.data());

    DataBufferPtr buf3 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf3->setData(map_coordinates.size() * sizeof(float) * 2, map_coordinates.data());

    // Создаем Terrain для рендеринга данных из буферов
    TerrainPtr mesh = std::make_shared<Terrain>(size, frequency, noise);

    // Добавляем в Mesh наши аттрибуты - координаты, нормали, tex и map
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, nullptr, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, nullptr, buf2);
    mesh->setAttribute(3, 2, GL_FLOAT, GL_FALSE, 0, nullptr, buf3);

    // Устанавливаем тип примитива - треугольник
    mesh->setPrimitiveType(GL_TRIANGLES);
    // Устанавливаем число вершин
    mesh->setVertexCount(static_cast<GLuint>(vertices.size()));
    return mesh;
}