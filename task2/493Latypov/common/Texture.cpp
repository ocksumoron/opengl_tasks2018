#include "Texture.hpp"

#include <SOIL2.h>

#include <iostream>


namespace {
    void invertY(unsigned char *image, int width, int height, int channels) {
        for (int j = 0; j * 2 < height; ++j) {
            auto index1 = static_cast<unsigned int>(j * width * channels);
            auto index2 = static_cast<unsigned int>((height - 1 - j) * width * channels);
            for (int i = 0; i < width * channels; i++) {
                unsigned char temp = image[index1];
                image[index1] = image[index2];
                image[index2] = temp;
                ++index1;
                ++index2;
            }
        }
    }
}

TexturePtr loadTexture(const std::string &filename, SRGB srgb) {
    int width, height, channels;
    unsigned char *image = SOIL_load_image(filename.c_str(), &width, &height, &channels, SOIL_LOAD_AUTO);
    if (!image) {
        std::cerr << "SOIL loading error: " << SOIL_last_result() << std::endl;
        return std::make_shared<Texture>();
    }

    invertY(image, width, height, channels);

    GLint internalFormat;
    if (srgb == SRGB::YES) {
        internalFormat = (channels == 4) ? GL_SRGB8 : GL_SRGB8_ALPHA8;
    } else {
        internalFormat = (channels == 4) ? GL_RGBA8 : GL_RGB8;
    }

    GLint format = (channels == 4) ? GL_RGBA : GL_RGB;

    TexturePtr texture = std::make_shared<Texture>(GL_TEXTURE_2D);
    texture->setTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height,
                           static_cast<GLenum>(format), GL_UNSIGNED_BYTE, image);
    texture->generateMipmaps();

    SOIL_free_image_data(image);
    return texture;
}