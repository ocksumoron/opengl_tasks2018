
/**
 Использование текстуры в качестве коэффициента отражения света
 */

#version 330

uniform sampler2D diffuseTex0;
uniform sampler2D diffuseTex1;
uniform sampler2D diffuseTex2;
uniform sampler2D diffuseTex3;
uniform sampler2D maskTex;

struct LightInfo
{
    vec3 pos; //положение источника света в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
    vec3 La; //цвет и интенсивность окружающего света
    vec3 Ld; //цвет и интенсивность диффузного света
    vec3 Ls; //цвет и интенсивность бликового света
};
uniform LightInfo light;

in vec3 normalCamSpace; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec4 posCamSpace; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec4 posAbsolute;
in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)
out vec4 fragColor; //выходной цвет фрагмента

const vec3 Ks = vec3(0.5, 0.5, 0.5); //Коэффициент бликового отражения
const float shininess = 32.0;

void main()
{
    // sand brick grass snow
    vec3 diffuseColor0 = texture(diffuseTex0, texCoord).rgb;
    vec3 diffuseColor1 = texture(diffuseTex1, texCoord).rgb;
    vec3 diffuseColor2 = texture(diffuseTex2, texCoord).rgb;
    vec3 diffuseColor3 = texture(diffuseTex3, texCoord).rgb;
    
    vec2 maskCoord = vec2(posAbsolute.x / 2.0, posAbsolute.y/ 2.0);
    vec3 maskColor = texture(maskTex, maskCoord).rgb;
  
//    with mask-texture
//    float snowRate = posAbsolute.z;
//    float grassRate = maskColor.b;
//    float brickRate = maskColor.g;
//    float sandRate = maskColor.r;
//
//    vec3 resultDiffuseColor = diffuseColor3 * snowRate
//        + max((1.0 - snowRate), 0.0) * (diffuseColor2 * grassRate + diffuseColor1 * brickRate  + diffuseColor1 * sandRate) * 0.8;
    
// with mask - heights
    float zpos = min(posAbsolute.z, 1.0);
    vec4 mask = vec4(1 / (abs(0.1 - zpos) + 0.001),
                     1 / (abs(0.3 - zpos) + 0.001),
                     1 / (abs(0.5 - zpos) + 0.001),
                     1 / (abs(1.0 - zpos) + 0.001));
    mask = normalize(mask);
    vec3 resultDiffuseColor =
        mask.r * texture(diffuseTex0, texCoord).rgb +
        mask.g * texture(diffuseTex1, texCoord).rgb +
        mask.b * texture(diffuseTex2, texCoord).rgb +
        mask.a * texture(diffuseTex3, texCoord).rgb;
    
    vec3 normal = normalize(normalCamSpace); //нормализуем нормаль после интерполяции
    vec3 viewDirection = normalize(-posCamSpace.xyz); //направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))
    
    vec3 lightDirCamSpace = normalize(light.pos - posCamSpace.xyz); //направление на источник света
    
    float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0); //скалярное произведение (косинус)
    
    vec3 color = resultDiffuseColor * (light.La + light.Ld * NdotL);
    
    if (NdotL > 0.0)
    {
        vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //биссектриса между направлениями на камеру и на источник света
        
        float blinnTerm = max(dot(normal, halfVector), 0.0); //интенсивность бликового освещения по Блинну
        blinnTerm = pow(blinnTerm, shininess); //регулируем размер блика
        color += light.Ls * Ks * blinnTerm * max(min(mask.a, 1.0), 0.0);
    }
    
    fragColor = vec4(color, 1.0);
}

