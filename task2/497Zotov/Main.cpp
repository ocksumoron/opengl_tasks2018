#include "Application.hpp"
#include "Mesh.hpp"
#include "ShaderProgram.hpp"
#include "PerlinNoise.h"
#include "Texture.hpp"
#include "LightInfo.hpp"

#include <iostream>
#include <functional>
#include <vector>
#include <cassert>
#include <random>

#define FINF 1e10

using std::vector;
using std::pair;

std::vector<glm::vec3> getTriangleNormals(const std::vector<glm::vec3> & vertices) {
    std::vector<glm::vec3> result;
    result.reserve(3);
    result.push_back(normalize(cross(vertices[2] - vertices[0], vertices[1] - vertices[0])));
    result.push_back(normalize(cross(vertices[0] - vertices[1], vertices[2] - vertices[1])));
    result.push_back(normalize(cross(vertices[1] - vertices[2], vertices[0] - vertices[2])));
    return result;
}

MeshPtr buildTerrainWithHeights(const vector<vector<float> > & heights, pair<float, float> xy_size) {
    assert(heights.size() > 0 && heights[0].size() > 0);
    
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;
    
    float x_step = xy_size.first / float(heights.size());
    float y_step = xy_size.second / float(heights[0].size());
    
    for(int ix = 0; ix < heights.size() - 1; ++ix) {
        float x_crd = ix * x_step;
        for(int iy = 0; iy < heights[0].size() - 1; ++iy) {
            float y_crd = iy * y_step;
            std::vector<glm::vec3> triangle;
            std::vector<glm::vec3> triangle_normals;
            
            glm::vec3 p1 = glm::vec3(x_crd, y_crd, heights[ix][iy]);
            glm::vec3 p2 = glm::vec3(x_crd + x_step, y_crd, heights[ix + 1][iy]);
            glm::vec3 p3 = glm::vec3(x_crd + x_step, y_crd + y_step, heights[ix + 1][iy + 1]);
            glm::vec3 p4 = glm::vec3(x_crd, y_crd + y_step, heights[ix][iy + 1]);
            
            // T1
            triangle = {p1, p3, p2};
            triangle_normals = getTriangleNormals(triangle);
            vertices.insert(vertices.end(), triangle.begin(), triangle.end());
            normals.insert(normals.end(), triangle_normals.begin(), triangle_normals.end());
            
            texcoords.push_back(glm::vec2(0.0, 1.0));
            texcoords.push_back(glm::vec2(1.0, 0.0));
            texcoords.push_back(glm::vec2(1.0, 1.0));
            
            // T2
            triangle = {p1, p4, p3};
            triangle_normals = getTriangleNormals(triangle);
            vertices.insert(vertices.end(), triangle.begin(), triangle.end());
            normals.insert(normals.end(), triangle_normals.begin(), triangle_normals.end());
            
            texcoords.push_back(glm::vec2(0.0, 1.0));
            texcoords.push_back(glm::vec2(0.0, 0.0));
            texcoords.push_back(glm::vec2(1.0, 0.0));
        }
    }
        
    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());
    
    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());
    
    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());
    
    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());
    
    std::cout << "Terrain is created with " << vertices.size() << " vertices\n";
    return mesh;
}

vector<vector< float > > generateHeightsWithPerlin(int x_size, int y_size, float scale) {
    vector<vector<float> > heights = vector<vector<float> >(x_size, vector<float>(y_size, 0.0));
    PerlinNoise pn;
    float max_val = -1e9, min_val = 1e9;
    for(int ix = 0; ix < heights.size(); ++ix) {
        for(int iy = 0; iy < heights[0].size(); ++iy) {
            heights[ix][iy] = pn.noise(scale * float(ix) / x_size, scale * float(iy) / y_size, 0.5);
            max_val = std::max(heights[ix][iy], max_val);
            min_val = std::min(heights[ix][iy], min_val);
        }
    }
    return heights;
}

vector<vector< float > > generateHeights(int x_size, int y_size, const vector<std::pair<int, int> > & top_points) {
    
    const double mean = 0.0;
    const double std = 0.04;
    
    std::default_random_engine generator;
    std::normal_distribution<double> dist(mean, std);
    vector<vector<float> > heights = vector<vector<float> >(x_size, vector<float>(y_size, 0.0));
    float min_val = 0.0;
    
    for(int ix = 1; ix < heights.size(); ++ix) {
        for(int iy = 1; iy < heights[0].size(); ++iy) {
            heights[ix][iy] = 0.3333 * (heights[ix-1][iy] + heights[ix][iy - 1] + heights[ix - 1][iy - 1])
            + dist(generator);
            
            for(int j = 0; j < top_points.size(); ++j) {
                int px = top_points[j].first;
                int py = top_points[j].second;
                
                heights[ix][iy] += 0.05 * (float)(px - ix) / (float)heights.size() / (float)top_points.size()
                                 + 0.05 * (float)(py - iy) / (float)heights[0].size() / (float)top_points.size();
            }
            
            min_val = std::min(heights[ix][iy], min_val);
        }
    }
    
    for(int ix = 0; ix < heights.size(); ++ix) {
        for(int iy = 0; iy < heights[0].size(); ++iy) {
            heights[ix][iy] -= min_val;
        }
    }
    return heights;
}
    
class TerrainApplication : public Application
{
public:
    MeshPtr _perlin_terrain;
    vector<vector< float > > _perlin_heights;
    glm::vec3 _perlin_terrain_position = glm::vec3(-10.0f, -10.0f, -0.3f);
    pair<float, float> _xy_size_perlin = std::make_pair(20.0 , 20.0);
    
    MeshPtr _terrain;
    vector<vector< float > > _heights;
    glm::vec3 _terrain_position = glm::vec3(0.0f, 0.0f , -1.0f);
    pair<float, float> _xy_size = std::make_pair(5.0 , 5.0);
    
    MeshPtr _bunny;
    MeshPtr _marker; //Маркер для источника света
    
    ShaderProgramPtr _shaderHeight;
    ShaderProgramPtr _shaderTexture;
    ShaderProgramPtr _markerShader;
    
    glm::vec3 _lightPosCamSpace;
    
    //Координаты источника света
    float _marker_dist = 5.0;
    glm::vec3 _marker_position;
    
    float _lr = 120.0;
    float _phi = 0.0;
    float _theta = glm::pi<float>() * 0.25f;
    
    TexturePtr _textures[5]; // sand brick grass snow earth

    GLuint _sampler;
    
    LightInfo _light;
    
    void makeScene() override
    {
        Application::makeScene();
        _cameraMover = std::make_shared<FreeCameraMover>();

        // Источник света
        _marker_position = glm::vec3(glm::cos(_phi) * glm::cos(_theta),
                                     glm::sin(_phi) * glm::cos(_theta),
                                     glm::sin(_theta)) * _marker_dist;
        
        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta),
                                       glm::sin(_phi) * glm::cos(_theta),
                                       glm::sin(_theta)) * _lr;
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(1.0, 1.0, 1.0);
        
        //=========================================================
        //Загрузка и создание текстур
        _textures[0] = loadTexture("497ZotovData/images/sand.jpg");
        _textures[1] = loadTexture("497ZotovData/images/brick.jpg");
        _textures[2] = loadTexture("497ZotovData/images/grass.jpg");
        _textures[3] = loadTexture("497ZotovData/images/snow.jpg");
        _textures[4] = loadTexture("497ZotovData/images/earth_global.jpg");
        
        //=========================================================
        //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
    
        
        // Perlin Terrain
        _perlin_heights = generateHeightsWithPerlin(300, 300, 20.0);
        _perlin_terrain = buildTerrainWithHeights(_perlin_heights, _xy_size_perlin);
        _perlin_terrain->setModelMatrix(glm::translate(glm::mat4(1.0f), _perlin_terrain_position));
        
        // My Terrain
        vector<std::pair<int, int> > top_points = {
            std::make_pair(50,40),
        };
        _heights = generateHeights(100, 100, top_points);
        _terrain = buildTerrainWithHeights(_heights, _xy_size);
        _terrain->setModelMatrix(glm::translate(glm::mat4(1.0f), _terrain_position));
        
        //Создаем меш из файла
        _bunny = loadFromFile("497ZotovData/models/bunny.obj");
        _bunny->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 3.0f, 0.0f)));
        
        // Маркер
        _marker = makeSphere(0.1f);
        
        //Создаем шейдерную программу
        _shaderHeight = std::make_shared<ShaderProgram>("497ZotovData/shaders/heightColor.vert",
                                                         "497ZotovData/shaders/heightColor.frag");
        _shaderTexture = std::make_shared<ShaderProgram>("497ZotovData/shaders/texture.vert",
                                                  "497ZotovData/shaders/texture.frag");
        _markerShader = std::make_shared<ShaderProgram>("497ZotovData/shaders/marker.vert",
                                                        "497ZotovData/shaders/marker.frag");
    }
    
    void updateGUI() override
    {
        Application::updateGUI();
        
        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);
            
            if (ImGui::CollapsingHeader("Light"))
            {
                ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
                ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));
                
//                ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
            }
            
//            if (ImGui::CollapsingHeader("Rabbit material"))
//            {
//                ImGui::ColorEdit3("mat ambient", glm::value_ptr(_bunnyAmbientColor));
//                ImGui::ColorEdit3("mat diffuse", glm::value_ptr(_bunnyDiffuseColor));
//            }
        }
        ImGui::End();
    }
    
    void update() override
    {
        double dt = glfwGetTime() - _oldTime;
        _oldTime = glfwGetTime();
        
        //-----------------------------------------
        _cameraMover->update(_window, dt);
        clamp_camera_position(std::dynamic_pointer_cast<FreeCameraMover>(_cameraMover)->_pos);
        _cameraMover->updateView(_window);
        _camera = _cameraMover->cameraInfo();
    }
    
    void clamp_camera_position(glm::vec3 & pos) {
        double z1 = clamp_z_with_heights(pos, _perlin_heights, _perlin_terrain_position, _xy_size_perlin);
        double z2 = clamp_z_with_heights(pos, _heights, _terrain_position, _xy_size);
        z1 = std::max(z1, z2);
        if(z1 > -FINF) {
            pos[2] = z1;
        }
    }
    
    float clamp_z_with_heights(const glm::vec3 & pos,
                              const vector<vector< float > > heights,
                              const glm::vec3 terrain_position,
                              pair<float, float> xy_size) const {
        // Относительные координаты
        double eps = 0.3;
        double x_pos = pos[0] - terrain_position[0];
        double y_pos = pos[1] - terrain_position[1];
        if(x_pos < 0.0 || x_pos >= xy_size.first) return -FINF;
        if(y_pos < 0.0 || y_pos >= xy_size.second) return -FINF;
        
        float xrate = (x_pos/xy_size.first) * heights.size();
        float yrate = (y_pos/xy_size.second) * heights[0].size();
        
        int ix = std::min(int(xrate), (int)heights.size() - 1);
        int iy = std::min(int(yrate), (int)heights[0].size() - 1);
        float tx = xrate - int(xrate);
        float ty = yrate - int(yrate);
        
        // sorry
//        int ixl = std::max(0, ix - 1);
        int ixr = std::min(ix + 1, (int)heights.size() - 1);
//        int iyl = std::max(0, iy - 1);
        int iyr = std::min(iy + 1, (int)heights[0].size() - 1);
        
        float h1 = heights[ix][iy];
        float h2 = heights[ixr][iy];
        float h3 = heights[ixr][iyr];
        float h4 = heights[ix][iyr];
        
        float h = (h1 * (1.0 - ty) + h4 * ty) * (1.0 - tx) + (h2 * (1.0 - ty) + h3 * ty) * tx;
        
        h += eps;
        // Возвращаем абсолютную высоту
        return h + terrain_position[2];
    }
    
    void draw_with_shader(const MeshPtr & mesh, const ShaderProgramPtr & shader) const {
        shader->setMat4Uniform("modelMatrix", mesh->modelMatrix());
        shader->setMat3Uniform("normalToCameraMatrix",
                                      glm::transpose(glm::inverse(
                                        glm::mat3(_camera.viewMatrix * mesh->modelMatrix()))));
        mesh->draw();
    }
    
    void init_shader_with_light(ShaderProgramPtr & shader) const {
        shader->use();
        
        shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        
        shader->setVec3Uniform("light.pos", _lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
        shader->setVec3Uniform("light.La", _light.ambient);
        shader->setVec3Uniform("light.Ld", _light.diffuse);
        shader->setVec3Uniform("light.Ls", _light.specular);
    }
    
    void draw() override
    {
        Application::draw();
        
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);
        
        glViewport(0, 0, width, height);
        
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        
        _lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));
    
        // shader height
        init_shader_with_light(_shaderHeight);
    
        draw_with_shader(_bunny, _shaderHeight);
    
        // shader texture
        init_shader_with_light(_shaderTexture);
        GLuint textureUnitForDiffuseTex[4] = {0,0,0,0};
        
        _marker_position = glm::vec3(glm::cos(_phi) * glm::cos(_theta),
                                     glm::sin(_phi) * glm::cos(_theta),
                                     glm::sin(_theta)) * _marker_dist;
        
        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta),
                                    glm::sin(_phi) * glm::cos(_theta),
                                    glm::sin(_theta)) * _lr;
        
        if (USE_DSA) {
            GLuint samplers[5] = { _sampler, _sampler, _sampler, _sampler, _sampler};
            glBindSamplers(0, 5, samplers);
            
            GLuint textures[5] = { _textures[0]->texture(),
                                    _textures[1]->texture(),
                                    _textures[2]->texture(),
                                    _textures[3]->texture(),
                                    _textures[4]->texture()
            };
            
            glBindTextures(0, 5, textures);
            
            _shaderTexture->setIntUniform("diffuseTex0", 0);
            _shaderTexture->setIntUniform("diffuseTex1", 1);
            _shaderTexture->setIntUniform("diffuseTex2", 2);
            _shaderTexture->setIntUniform("diffuseTex3", 3);
            _shaderTexture->setIntUniform("maskTex", 4);
        }
        else {
            glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
            glBindSampler(0, _sampler);
            _textures[0]->bind();
            _shaderTexture->setIntUniform("diffuseTex0", 0);
            
            glActiveTexture(GL_TEXTURE1);  //текстурный юнит 1
            glBindSampler(1, _sampler);
            _textures[1]->bind();
            _shaderTexture->setIntUniform("diffuseTex1", 1);
            
            glActiveTexture(GL_TEXTURE2);  //текстурный юнит 2
            glBindSampler(2, _sampler);
            _textures[2]->bind();
            _shaderTexture->setIntUniform("diffuseTex2", 2);
            
            glActiveTexture(GL_TEXTURE3);  //текстурный юнит 3
            glBindSampler(3, _sampler);
            _textures[3]->bind();
            _shaderTexture->setIntUniform("diffuseTex3", 3);
            
            glActiveTexture(GL_TEXTURE4);  //текстурный юнит 4
            glBindSampler(4, _sampler);
            _textures[4]->bind();
            _shaderTexture->setIntUniform("maskTex", 4);
        }
        
        // Perlin
        draw_with_shader(_perlin_terrain, _shaderTexture);
        
        // Random Walk
        draw_with_shader(_terrain, _shaderTexture);
        

        //Рисуем маркер для источника света
        {
            _markerShader->use();
            
            _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _marker_position));
            _markerShader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
            _marker->draw();
        }
        glBindSampler(0, 0);
        glUseProgram(0);
    }
};

int main()
{
    TerrainApplication app;
    app.start();
    
    return 0;
}
